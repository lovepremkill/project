const express = require('express');
const router = express.Router();

const indexController = require('../controllers/indexController');

router.get('/home',indexController.index);
router.get('/homee',indexController.home);

module.exports = router;
