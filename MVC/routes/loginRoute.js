const express = require('express');
const router = express.Router();

const homeController = require('../controllers/loginController');
const validator = require('../controllers/loginValidator');

router.get('/', homeController.log);
router.post('/login/admin', validator.loginadmin, homeController.loginadmin);
router.post('/login/teacher', validator.loginteacher, homeController.loginteacher);
router.post('/login/student', validator.loginastudent, homeController.loginstudent);
router.get('/home', homeController.home);
router.get('/logout', homeController.logout);


module.exports = router;