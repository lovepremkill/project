const express = require('express');
const router = express.Router();
const adminstudentController = require('../controllers/adminstudentController');
const validatorcheck = require ('../controllers/adminstudentValidator');

router.get('/adminstudent',adminstudentController.list);
router.post('/adminstudent/save',validatorcheck.addValidator,adminstudentController.save);
router.get('/adminstudent/delete/:id',adminstudentController.delete);
router.get('/adminstudent/del/:id',adminstudentController.del);
router.get('/adminstudent/edit/:id',validatorcheck.addValidator,adminstudentController.edit);
router.post('/adminstudent/update/:id',validatorcheck.addValidator,adminstudentController.update);
router.get('/adminstudent/add',adminstudentController.add);



module.exports = router;
