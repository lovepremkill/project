const express = require('express');
const router = express.Router();

const uiteacherController = require('../controllers/uiteacherController');
const uiteacherValidator = require('../controllers/uiteacherValidator');
router.get('/uiteacher',uiteacherController.list);
router.get('/uiteacher/detail/:id',uiteacherController.detail);
router.get('/uiteacher/del/:id',uiteacherController.del);
router.get('/uiteacher/delete/:id',uiteacherController.delete);
router.get('/uiteacher/deletestudent/:id',uiteacherController.deletestudent);
router.get('/uiteacher/add',uiteacherController.add);
router.get('/uiteacher/addstudent/:id',uiteacherValidator.uiteacherValidator,uiteacherController.addstudent);
router.get('/uiteacher/studentdelete/:id',uiteacherValidator.uiteacherValidator,uiteacherController.delstudent);
router.post('/uiteacher/savestudent/:id',uiteacherValidator.uiteacherValidator,uiteacherController.savestudent);
router.post('/uiteacher/addstudent2/:id',uiteacherValidator.uiteacherValidator,uiteacherController.addstudent2);
router.post('/uiteacher/addstudent3/:id',uiteacherValidator.uiteacherValidator,uiteacherController.addstudent3);
router.post('/uiteacher/update/:id',uiteacherValidator.addValidator,uiteacherController.update);
router.get('/uiteacher/edit/:id',uiteacherController.edit);
router.post('/uiteacher/save',uiteacherValidator.addValidator,uiteacherController.save);
router.get('/uiteacher/upload/:id',uiteacherController.upload);///////////////////////////////////////////
router.post('/uiteacher/uploadUp/:id/:key',uiteacherController.uploadUp);///////////////////////////////////////////
//router.post('/uiteacher/addstudent2/:id',uiteacherValidator.addValidator,uiteacherController.addstudent2);
module.exports = router;
