const express = require('express');
const router = express.Router();
const s6123Controller = require('../controllers/uistudentController');
const uistudentValidator = require('../controllers/uistudentValidator');
router.get('/uistudent',s6123Controller.list);
router.get('/uistudent/add',s6123Controller.add);
router.post('/uistudent/save/',uistudentValidator.addValidator,s6123Controller.save);
router.get('/uistudent/listAddStudent/:id',s6123Controller.listAddStudent);
router.get('/uistudent/toDepartment',s6123Controller.toDepartment);
router.get('/uistudent/toMajor/:idDp',s6123Controller.toMajor);
router.get('/uistudent/toStudent/:idMj',s6123Controller.toStudent);
router.get('/uistudent/toSave/:idSt',s6123Controller.saveStudent);
router.get('/uistudent/delete/:id',s6123Controller.delete);
router.get('/uistudent/deletelist/:id',s6123Controller.deletelist);
router.get('/uistudent/update/:id',s6123Controller.edit);
router.post('/uistudent/update/:id',uistudentValidator.addValidator,s6123Controller.update);
router.get('/uistudent/upload/:id',s6123Controller.upload);///////////////////////////////////////////
router.post('/uistudent/uploadUp/:id/:key',s6123Controller.uploadUp);///////////////////////////////////////////
module.exports = router;