const express = require('express');
const router = express.Router();
const uilist = require('../controllers/uilistController');
const validator = require('../controllers/uilistValidator');


router.get('/',uilist.list);
router.post('/uilist/search',uilist.search);

module.exports = router;
