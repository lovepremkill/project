const express = require('express');
const router = express.Router();

const majorController = require('../controllers/adminDepartmentController');
const majorValidator = require('../controllers/adminDepartmentValidator');

router.get('/adminDepartment', majorController.list);
router.get('/adminDepartment/add', majorController.add);
router.post('/adminDepartment/save', majorValidator.addValidator, majorController.save);

router.get('/adminDepartment/del/:id', majorController.del);
router.get('/adminDepartment/delete/:id', majorController.delete);

router.get('/adminDepartment/edit/:id', majorController.edit);
router.post('/adminDepartment/update/:id', majorValidator.updateValidator, majorController.update);


module.exports = router;