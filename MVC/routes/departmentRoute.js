const express = require('express');
const router = express.Router();

const majorController = require('../controllers/departmentController');

router.get('/department', majorController.list);

module.exports = router;