const express = require('express');
const router = express.Router();

const majorController = require('../controllers/majorController');

router.get('/major', majorController.list);

module.exports = router;