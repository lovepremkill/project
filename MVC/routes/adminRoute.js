

const express = require('express');
const router = express.Router();
const admin = require('../controllers/adminController');
const validator = require('../controllers/adminValidator');


router.get('/admin',admin.list);
router.post('/admin/save',validator.addValidator,admin.save);
router.get('/admin/delete/:id',admin.delete);
router.get('/admin/del/:id',admin.del);
router.get('/admin/update/:id',admin.edit);
router.post('/admin/update/:id',validator.editValidator,admin.update);
router.get('/admin/add',admin.add);
module.exports = router;
