const express = require('express');
const router = express.Router();
const teachertype = require('../controllers/teachertypeController');
const validator = require('../controllers/teachertypeValidator');

router.get('/teachertype',teachertype.list);
router.post('/teachertype/save',validator.addValidator,teachertype.save);
router.get('/teachertype/delete/:id',teachertype.delete);
router.get('/teachertype/del/:id',teachertype.del);
router.get('/teachertype/update/:id',teachertype.edit);
router.post('/teachertype/update/:id',validator.updateValidator,teachertype.update);
router.get('/teachertype/add',teachertype.add);

module.exports = router;
