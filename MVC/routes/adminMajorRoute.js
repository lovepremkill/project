const express = require('express');
const router = express.Router();

const majorController = require('../controllers/adminMajorController');
const majorValidator = require('../controllers/adminMajorValidator');

router.get('/adminMajor', majorController.list);
router.get('/adminMajor/add', majorController.add);
router.post('/adminMajor/save', majorValidator.addValidator, majorController.save);

router.get('/adminMajor/del/:id', majorController.del);
router.get('/adminMajor/delete/:id', majorController.delete);

router.get('/adminMajor/edit/:id', majorController.edit);
router.post('/adminMajor/update/:id', majorValidator.updateValidator, majorController.update);


module.exports = router;