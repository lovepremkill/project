const express = require('express');
const router = express.Router();
const validator = require('../controllers/uiadminteacherValidator');
const uiadminteacherController = require('../controllers/uiadminteacherController');


router.get('/uiadminteacher', uiadminteacherController.list);
router.get('/uiadminteacher/add', uiadminteacherController.add);
router.post('/uiadminteacher/save', validator.addValidator, uiadminteacherController.save);
router.get('/uiadminteacher/edit/:id', uiadminteacherController.edit);
router.post('/uiadminteacher/update/:id', validator.updateValidator, uiadminteacherController.update);
router.get('/uiadminteacher/delete/:id', uiadminteacherController.delete);
//router.get('/uiadminteacher/report/:id',uiadminteacherController.report);
router.get('/uiadminteacher/del/:id', uiadminteacherController.del);
//router.get('/uiadminteacher/delete/:id',uiadminteacherController.delete);
//router.get('/uiadminteacher/edit/:id',uiadminteacherController.edit);
//router.post('/uiadminteacher/update/:id',uiadminteacherController.update);*

module.exports = router;