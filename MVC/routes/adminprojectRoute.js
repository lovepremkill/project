const express = require('express');
const router = express.Router();

const adminprojectcontroller = require('../controllers/adminprojectController');
const adminprojectValidator =require('../controllers/adminprojectValidator');

router.get('/adminproject',adminprojectcontroller.list);
router.post('/adminproject/save',adminprojectValidator.addValidator,adminprojectcontroller.save);
router.get('/adminproject/delete/:id',adminprojectcontroller.delete);
router.get('/adminproject/deletestudent/:id',adminprojectcontroller.deletestudent);
router.get('/adminproject/del/:id',adminprojectcontroller.del);
router.get('/adminproject/edit/:id',adminprojectcontroller.edit);
router.post('/adminproject/update/:id',adminprojectValidator.editValidator,adminprojectcontroller.update);
router.get('/adminproject/add/',adminprojectcontroller.add);
router.get('/adminproject/addstudent/:id',adminprojectValidator.addStudentValidator1,adminprojectcontroller.addstudent);
router.get('/adminproject/studentdelete/:id',adminprojectcontroller.delstudent);
router.post('/adminproject/savestudent/:id',adminprojectcontroller.savestudent);
router.post('/adminproject/addstudent2/:id',adminprojectValidator.addStudentValidator2,adminprojectcontroller.addstudent2);
router.post('/adminproject/addstudent3/:id',adminprojectValidator.addStudentValidator3,adminprojectcontroller.addstudent3);
router.get('/adminproject/report/:id',adminprojectcontroller.report);
router.get('/adminproject/upload/:id',adminprojectcontroller.upload);
router.post('/adminproject/uploadUp/:id/:key',adminprojectcontroller.uploadUp);
module.exports = router;
