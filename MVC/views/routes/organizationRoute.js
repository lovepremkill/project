const express = require('express');
const router = express.Router();
const organizationController = require('../controllers/organizationController');
const validatorcheck = require ('../controllers/organizationValidator');

router.get('/organization',organizationController.list);
router.post('/organization/save',validatorcheck.addValidator,organizationController.save);
router.get('/organization/delete/:id',organizationController.delete);
router.get('/organization/del/:id',organizationController.del);
router.get('/organization/update/:id',validatorcheck.addValidator,organizationController.edit);
router.post('/organization/update/:id',validatorcheck.addValidator,organizationController.update);
router.get('/organization/add',organizationController.add);



module.exports = router;
