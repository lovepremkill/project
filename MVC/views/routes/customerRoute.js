const express = require('express');
const router = express.Router();

const customerController = require('../Controllers/customerController');
const customerValidator = require('../controllers/customerValidator');

router.get('/customer', customerController.list);
router.get('/customer/add', customerController.add);
router.post('/customer/save', customerValidator.addValidator, customerController.save);

router.get('/customer/del/:id', customerController.del);
router.get('/customer/delete/:id', customerController.delete);

router.get('/customer/edit/:id', customerController.edit);
router.post('/customer/update/:id', customerValidator.updateValidator, customerController.update);


module.exports = router;