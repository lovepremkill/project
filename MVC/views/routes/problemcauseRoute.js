const express = require('express');
const router = express.Router();
const problemcause = require('../controllers/problemcauseController');
const validator = require('../controllers/problemcauseValidator');

router.get('/problemcause', problemcause.list);
router.post('/problemcause/save/:id',validator.addValidator, problemcause.save);
router.get('/problemcause/delete/:id', problemcause.delete);
router.get('/problemcause/del/:id', problemcause.del);
router.get('/problemcause/update/:id', problemcause.edit);
router.post('/problemcause/update/:id',validator.updateValidator, problemcause.update);
router.get('/problemcause/add/:id', problemcause.add);
router.get('/problemcause/report/:id',problemcause.report);

module.exports = router;
