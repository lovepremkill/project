const express = require('express');
const router = express.Router();

const meetingcontroller = require('../controllers/meetingController');
const meetingValidator =require('../controllers/meetingValidator');

router.get('/meeting',meetingcontroller.list);
router.post('/meeting/save/:id',meetingcontroller.save);
router.get('/meeting/delete/:id',meetingcontroller.delete);
router.get('/meeting/del/:id',meetingcontroller.del);
router.get('/meeting/edit/:id',meetingcontroller.edit);
router.post('/meeting/update/:id',meetingcontroller.update);
router.get('/meeting/add/:id',meetingcontroller.add);
router.get('/meeting/report/:id',meetingcontroller.report);

module.exports = router;
