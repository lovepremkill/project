const express = require('express');
const router = express.Router();
const getdeviceController = require('../controllers/getdeviceController');
const validator = require('../controllers/getdeviceValidator');

router.get('/getdevice',getdeviceController.list);
router.get('/getdevice/show/:id',getdeviceController.show);
router.get('/getdevice/add/:id',getdeviceController.add);
router.post('/getdevice/save/:id',validator.addValidator,getdeviceController.save);
router.get('/getdevice/del/:id',getdeviceController.del);
router.get('/getdevice/delete/:id',getdeviceController.delete);
router.get('/getdevice/edit/:id',getdeviceController.edit);
router.post('/getdevice/update/:id',validator.updateValidator,getdeviceController.update);

module.exports = router;
