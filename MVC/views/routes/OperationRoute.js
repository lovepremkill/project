const express = require('express');
const router = express.Router();

const operationController = require('../controllers/operationController');
const operationValidator = require('../controllers/operationValidator');


router.get('/operation',operationController.list);
router.get('/operation/add/:id',operationController.add);
router.post('/operation/save/:id',operationValidator.addValidator,operationController.save);
router.get('/operation/report/:id',operationController.report);
router.get('/operation/del/:id',operationController.del);
router.get('/operation/delete/:id',operationController.delete);
router.get('/operation/edit/:id',operationController.edit);
router.post('/operation/update/:id',operationValidator.updateValidator,operationController.update);

module.exports = router;
