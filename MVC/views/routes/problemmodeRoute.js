

const express=require('express');
const router=express.Router();

const pmode =require("../controllers/problemmodeController");
const validator =require('../controllers/problemmodeValidator');

router.get('/problemmode',pmode.list);
router.post('/problemmode/save/:id',validator.addValidator,pmode.save);
router.get('/problemmode/del/:id',pmode.del);
router.get('/problemmode/delete/:id',pmode.delete);
router.get('/problemmode/update/:id',pmode.edit);
router.post('/problemmode/update/:id',validator.editValidator,pmode.update);
router.get('/problemmode/add/:id',pmode.add);
router.get('/problemmode/report/:id',pmode.report);

module.exports=router;
