const express = require('express');
const router = express.Router();

const recallsController = require('../controllers/recallController');
const recallsValidator = require('../controllers/recallValidator');

router.get('/recall',recallsController.list);
router.get('/recall/add/:id',recallsController.add);
router.post('/recall/save/:id',recallsValidator.addValidator,recallsController.save);
router.get('/recall/report/:id',recallsController.report);
router.get('/recall/del/:id',recallsController.del);
router.get('/recall/delete/:id',recallsController.delete);
router.get('/recall/edit/:id',recallsController.edit);
router.post('/recall/update/:id',recallsValidator.updateValidator,recallsController.update);

module.exports = router;  