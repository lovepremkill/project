const express = require('express');
const router = express.Router();

const sController = require('../controllers/problemlistController');
const validator =require('../controllers/problemlistValidator');

router.get('/problemlist',sController.list);
router.get('/problemlist/add',sController.add);
router.post('/problemlist/save',validator.add,sController.save);

router.get('/problemlist/del/:id',sController.del);
router.get('/problemlist/delete/:id',sController.delete);

router.get('/problemlist/edit/:id',sController.edit);
router.post('/problemlist/update/:id',validator.update,sController.update );


module.exports = router;
