const express = require('express');
const router = express.Router();
const closeController = require('../controllers/closeproblemController');
const closeValidator = require('../controllers/closeproblemValidator');
router.get('/closeproblem',closeController.list);
router.get('/closeproblem/add/:id',closeController.add);
router.post('/closeproblem/save/:id',closeValidator.addValidator,closeController.save);
router.get('/closeproblem/report/:id',closeController.report);
//router.get('/closeproblem/report/:id',closeController.report);
router.get('/closeproblem/del/:id',closeController.del);
router.get('/closeproblem/delete/:id',closeController.delete);
router.get('/closeproblem/edit/:id',closeController.edit);
router.post('/closeproblem/update/:id',closeController.update);

module.exports = router;
