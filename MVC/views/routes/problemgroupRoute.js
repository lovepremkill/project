const express = require('express');
const router = express.Router();

const sController = require('../controllers/problemgroupController');
//const validator =require('../controllers/problemgroupValidator');

router.get('/problemgroup',sController.list);
// router.get('/problemgroup/add',sController.add);
// router.post('/problemgroup/save',sController.save);

// router.get('/problemgroup/del/:id',sController.del);
// router.get('/problemgroup/delete/:id',sController.delete);

// router.get('/problemgroup/edit/:id',sController.edit);
// router.post('/problemgroup/update/:id',sController.update );


module.exports = router;
