const express = require('express');
const router = express.Router();

const itsupController = require('../controllers/itsupportController');
const itsupValidator = require('../controllers/itsupportValidator');

router.get('/itsupport',itsupController.list);
router.get('/itsupport/add',itsupController.add);
router.post('/itsupport/save',itsupValidator.addValidator,itsupController.save);
router.get('/itsupport/del/:id',itsupController.del);
router.get('/itsupport/delete/:id',itsupController.delete);
router.get('/itsupport/edit/:id',itsupController.edit);
router.post('/itsupport/update/:id',itsupValidator.updateValidator,itsupController.update);

module.exports = router;
