const express = require('express');
const router = express.Router();

const fixITController = require('../controllers/fixITController');
const validatorlogin =require('../controllers/fixITValidator');
const reportValidator= require('../controllers/problemreportValidator');

router.get('/',fixITController.index);
router.post('/savereport',reportValidator.ReportValidator,fixITController.save);
router.get('/login',fixITController.loginForm);
router.post('/login',validatorlogin.passValidator,fixITController.login);
router.get('/home',fixITController.home);
router.get('/homeCustomer',fixITController.homeCustomer);
router.get('/logout',fixITController.logout);

module.exports = router;
