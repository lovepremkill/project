const express = require('express');
const router = express.Router();

const reportController = require('../controllers/problemreportController');
const reportValidator = require('../controllers/problemreportValidator');
router.get('/report/add',reportController.add);
router.get('/report/add2',reportController.add2);
router.get('/report',reportController.list);
router.post('/report/save',reportValidator.ReportValidator,reportController.save);
router.get('/report/del/:id',reportController.del);
router.get('/report/delete/:id',reportController.delete);
router.get('/report/detail/:id',reportController.detail);
router.get('/report/edit/:id',reportController.edit);
router.post('/report/update/:id',reportValidator.ReportValidator,reportController.update);
module.exports = router;
