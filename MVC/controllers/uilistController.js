const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('select   st.lastname as lastname,st.studentcode as studentcode,st.id as idst,pj.id as idpj,mj.id as idmj,pj.nameen as engname,pj.nameth as thname,st.firstname as fname,st.lastname as lname,mj.name as mjname,pj.year as yearmj,t.name as tname from studentproject as spro left join project as pj on spro.project_id=pj.id left join  student as st on spro.student_id=st.id left join major as mj on st.major_id=mj.id left join teacher as t on pj.teacher_id=t.id ;',
            (err, uilist) => {
                conn.query('select * from major ;', (err2, major) => {
                    if (err) {
                        res.json(err);
                    }
                    res.render('uilist/uiList', {
                        session: req.session,
                        data2: major,
                        data: uilist
                    });
                })
            });
    });
};




controller.search = (req, res) => {
    const { idmj } = req.params;

    const data = req.body;
    const x = data.major_id;
    const y = data.year;
    if (data.major_id == "") {
        if (data.year == "") {
            req.getConnection((err, conn) => {
                conn.query('select   st.lastname as lastname,st.studentcode as studentcode,st.id as idst,pj.id as idpj,mj.id as idmj,pj.nameen as engname,pj.nameth as thname,st.firstname as fname,st.lastname as lname,mj.name as mjname,pj.year as yearmj,t.name as tname from studentproject as spro left join project as pj on spro.project_id=pj.id left join  student as st on spro.student_id=st.id left join major as mj on st.major_id=mj.id left join teacher as t on pj.teacher_id=t.id ;',
                    (err, uilist) => {
                        conn.query('select * from major ;', (err2, major) => {
                            if (err) {
                                res.json(err);
                            }
                            res.render('uilist/uiList', {
                                  session: req.session,
                                data2: major,
                                data: uilist

                            });
                        })
                    });
            });
        } else {
            req.getConnection((err, conn) => {
                conn.query('select   st.lastname as lastname,st.studentcode as studentcode,st.id as idst,pj.id as idpj,mj.id as idmj,pj.nameen as engname,pj.nameth as thname,st.firstname as fname,st.lastname as lname,mj.name as mjname,pj.year as yearmj,t.name as tname from studentproject as spro left join project as pj on spro.project_id=pj.id left join  student as st on spro.student_id=st.id left join major as mj on st.major_id=mj.id left join teacher as t on pj.teacher_id=t.id where pj.year=?;',
                    [y], (err, uilist) => {
                        conn.query('select * from major ;', (err2, major) => {
                            if (err) {
                                res.json(err);
                            }
                            res.render('uilist/uiList', {
                                session: req.session,
                                data2: major,
                                data: uilist

                            });
                        })
                    });
            });
        }
    } else {
        if (data.year == "") {
            req.getConnection((err, conn) => {
                conn.query('select   st.lastname as lastname,st.studentcode as studentcode,st.id as idst,pj.id as idpj,mj.id as idmj,pj.nameen as engname,pj.nameth as thname,st.firstname as fname,st.lastname as lname,mj.name as mjname,pj.year as yearmj,t.name as tname from studentproject as spro left join project as pj on spro.project_id=pj.id left join  student as st on spro.student_id=st.id left join major as mj on st.major_id=mj.id left join teacher as t on pj.teacher_id=t.id where st.major_id=? ;',
                    [x], (err, uilist) => {
                        conn.query('select * from major ;', (err2, major) => {
                            if (err) {
                                res.json(err);
                            }
                            res.render('uilist/uiList', {
                                session: req.session,
                                data2: major,
                                data: uilist

                            });
                        })
                    });
            });
        } else {
            req.getConnection((err, conn) => {
                conn.query('select   st.lastname as lastname,st.studentcode as studentcode,st.id as idst,pj.id as idpj,mj.id as idmj,pj.nameen as engname,pj.nameth as thname,st.firstname as fname,st.lastname as lname,mj.name as mjname,pj.year as yearmj,t.name as tname from studentproject as spro left join project as pj on spro.project_id=pj.id left join  student as st on spro.student_id=st.id left join major as mj on st.major_id=mj.id left join teacher as t on pj.teacher_id=t.id where st.major_id=? and pj.year=? ;',
                    [x, y], (err, uilist) => {
                        conn.query('select * from major ;', (err2, major) => {
                            if (err) {
                                res.json(err);
                            }
                            res.render('uilist/uiList', {
                                session: req.session,
                                data2: major,
                                data: uilist

                            });
                        })
                    });
            });

        }
    }
};


// controller.save = (req, res) => {
//     const data = req.body;
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//         req.session.errors = errors;
//         req.session.success = false;
//         res.redirect('/teachertype/add');
//     } else {
//         req.session.success = true;
//         req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
//         req.getConnection((err, conn) => {
//             conn.query('INSERT INTO teachertype set ?', [data], (err, ce611998039) => {
//                 res.redirect('/teachertype');
//             });
//         });
//     };

// };

// controller.del = (req, res) => {
//     const { id } = req.params;

//     req.getConnection((err, conn) => {
//         conn.query('select * from teachertype HAVING id = ?', [id], (err, ce611998039) => {
//             if (err) {
//                 res.json(err);
//             }
//             res.render('teachertype/teachertypeDelete', { session: req.session, data: ce611998039[0] });
//         });
//     });
// };

// controller.delete = (req, res) => {
//     const { id } = req.params;

//     req.getConnection((err, conn) => {
//         conn.query('DELETE FROM teachertype WHERE id= ?', [id], (err, ce611998039) => {
//             if (err) {
//                 const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
//                 req.session.errors = errorss;
//                 req.session.success = false;
//             } else {
//                 req.session.success = true;
//                 req.session.topic = "ลบข้อมูลเสร็จแล้ว";
//             }
//             console.log(ce611998039);
//             res.redirect('/teachertype');
//         });
//     });
// };

// controller.edit = (req, res) => {
//     const { id } = req.params;
//     req.getConnection((err, conn) => {
//         conn.query('SELECT * FROM teachertype WHERE id= ?', [id], (err, ce611998039) => {
//             res.render('teachertype/teachertypeUpdate', {
//                 session: req.session,
//                 data1: ce611998039[0],
//             });
//         });
//     });
// }
// controller.update = (req, res) => {
//     const errors = validationResult(req);
//     const { id } = req.params;
//     const data = req.body;
//     if (!errors.isEmpty()) {
//         req.session.errors = errors;
//         req.session.success = false;

//         req.getConnection((err, conn) => {
//             conn.query('SELECT * FROM teachertype WHERE id= ?', [id], (err, ce611998039) => {

//                 res.render('teachertype/teachertypeUpdate', {
//                     session: req.session,
//                     data1: ce611998039[0],
//                 });

//             });
//         });

//     } else {
//         req.session.success = true;
//         req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
//         req.getConnection((err, conn) => {
//             conn.query('UPDATE  teachertype SET ?  WHERE id = ?', [data, id], (err, ce611998039) => {
//                 if (err) {
//                     res.json(err);
//                 }
//                 res.redirect('/teachertype');
//             });
//         });
//     }
// };

// controller.add = (req, res) => {
//     req.getConnection((err, conn) => {
//         conn.query('SELECT * FROM teachertype', (err, ce611998018) => {
//             res.render('teachertype/teachertypeAdd', { data: ce611998018, session: req.session });
//         });
//     });
// };
module.exports = controller;
