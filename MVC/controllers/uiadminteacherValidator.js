const { check } = require('express-validator');

exports.addValidator = [
    check('name', "กรุณากรอก ชื่อ !").not().isEmpty(),
    check('jobposition', "กรุณากรอก ตำแหน่ง !").not().isEmpty(),
    check('username', "กรุณากรอก ไอดี !").not().isEmpty(),
    check('password', "กรุณากรอก รหัสผ่าน !").not().isEmpty(),
    check('email', "กรุณากรอก อีเมลล์ !").not().isEmpty()
];

exports.updateValidator = [
    check('name', "กรุณากรอก ชื่อ !").not().isEmpty(),
    check('jobposition', "กรุณากรอก ตำแหน่ง !").not().isEmpty(),
    check('username', "กรุณากรอก ไอดี !").not().isEmpty(),
    check('password', "กรุณากรอก รหัสผ่าน !").not().isEmpty(),
    check('email', "กรุณากรอก อีเมลล์ !").not().isEmpty()
];