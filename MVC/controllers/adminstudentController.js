const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
      if(req.session.admin){
        req.getConnection((err, conn) => {
            conn.query('select s.id as sid , s.firstname as sfirstname, s.lastname as slastname , s.phone as sphone , s.studentcode as sstudentcode , s.password as spassword , m.name as mname , d.name as dname ,DATE_FORMAT(birthday,"%d/%m/%y%y") as sbirthday , s.address as saddress , s.email as semail   FROM  student as s left JOIN major as m ON s.major_id = m.id left JOIN department as d ON m.department_id = d.id  ', (err, adminstudent) => {
                if (err) {
                    res.json(err);
                }
                res.render('adminstudent/adminstudentList', {
                    data: adminstudent,

                    session: req.session

                });
            });
        });
      } else {
          res.redirect('/');
      };

};


//*****************************************************************************************************


controller.save = (req,res) => {
if(req.session.admin){
    console.log("save");
      const data=req.body;
      if(data.major_id==""){data.major_id = null;}
      const errors = validationResult(req);
          if(!errors.isEmpty()){
              req.session.errors=errors;
              req.session.success=false;
              res.redirect('/adminstudent/add');
          }else{
              req.session.success=true;
              req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                  conn.query('INSERT INTO student set ?',[data],(err,adminstudent)=>{

                      res.redirect('/adminstudent');
                  });
              });
          }
        } else {
            res.redirect('/');
        };
  };


//*********************************************************************************************************
controller.delete = (req, res) => {
  if(req.session.admin){
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('DELETE FROM student  WHERE id = ?', [id], (err, adminstudent) => {
                if (err) {
                    const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้ ', param: '', location: '' }] }
                    req.session.errors = errorss;
                    req.session.success = false;
                } else {
                    req.session.success = true;
                    req.session.topic = "ลบข้อมูลสำเร็จ";

                };
                console.log(adminstudent);
                res.redirect('/adminstudent');
            });
        });
      } else {
          res.redirect('/');
      };

};

//************************************************************************************************************

controller.del = (req, res) => {
  if(req.session.admin){
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT s.id as sid , s.firstname as sfirstname, s.lastname as slastname , s.phone as sphone , s.studentcode as sstudentcode , s.password as spassword , m.name as mname , d.name as dname , DATE_FORMAT(birthday,"%d/%m/%y%y") as sbirthday , s.address as saddress , s.email as semail   FROM  student as s left JOIN major as m ON s.major_id = m.id left JOIN department as d ON m.department_id = d.id HAVING s.id= ?', [id], (err, adminstudent) => {
                if (err) {
                    res.json(err);
                }

                res.render('adminstudent/adminstudentDel', {
                    data: adminstudent[0],
                    session: req.session

                });
            });
        });
      } else {
          res.redirect('/');
      };

};

//*****************************************************************************************************************

controller.edit = (req, res) => {
  if(req.session.admin){
        const { id } = req.params;
        req.getConnection((err, conn) => {
          conn.query('SELECT s.major_id as smajor_id , s.id as sid , s.firstname as sfirstname, s.lastname as slastname , s.phone as sphone , s.studentcode as sstudentcode , s.password as spassword , m.name as mname , d.name as dname ,DATE_FORMAT(birthday,"%y%y-%m-%d") as sbirthday , s.address as saddress , s.email as semail   FROM  student as s left JOIN major as m ON s.major_id = m.id left JOIN department as d ON m.department_id = d.id  WHERE s.id = ? ;', [id], (err, student) => {
            conn.query('SELECT * FROM major  ;', [id], (err, major) => {
              conn.query('SELECT * FROM department ;', [id], (err, department) => {




                if (err) {
                    res.json(err);
                  } else {

                      res.render("adminstudent/adminstudentUpdate", {
                          data: student[0],
                          data2: major,
                          data3:department,
                          session: req.session
                      });
                  };
              });
              });
          });
      });
    } else {
        res.redirect('/');
    };
  };


//*******************************************************************************************************************

controller.update = function(req, res) {
if(req.session.admin){
        const { id } = req.params;
        const data = req.body;
        const errors = validationResult(req);
        if (data.major_id == "") { data.major_id = null; };
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            req.getConnection((err, conn) => {
                conn.query(' SELECT s.major_id as smajor_id , s.id as sid , s.firstname as sfirstname, s.lastname as slastname , s.phone as sphone , s.studentcode as sstudentcode , s.password as spassword , m.name as mname , d.name as dname ,DATE_FORMAT(birthday,"%y%y-%m-%d") as sbirthday , s.address as saddress , s.email as semail   FROM  student as s left JOIN major as m ON s.major_id = m.id left JOIN department as d ON m.department_id = d.id  WHERE sid = ?', [id], function(err, student) {
                  conn.query(' SELECT s.major_id as smajor_id , s.id as sid , s.firstname as sfirstname, s.lastname as slastname , s.phone as sphone , s.studentcode as sstudentcode , s.password as spassword , m.name as mname , d.name as dname ,DATE_FORMAT(birthday,"%y%y-%m-%d") as sbirthday , s.address as saddress , s.email as semail   FROM  student as s left JOIN major as m ON s.major_id = m.id left JOIN department as d ON m.department_id = d.id  WHERE sid = ?', [id], function(err, major) {

                        res.render('adminstudent/adminstudentUpdate', {
                            session: req.session,
                            data: student,
                            data2:major


                        });
                      });
                });
            });
        } else {

            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection(function(err, conn) {
                conn.query('update student set ? where id = ?', [data, id], function(err, adminstudent) {
                  console.log(adminstudent);
                    if (err) {
                        console.log(err);

                    }
                    res.redirect('/adminstudent')
                });
            });
        };
      } else {
          res.redirect('/');
      };
};



//*************************************************************************************************************

controller.add = function(req, res) {
  if(req.session.admin){
        req.getConnection(function(err, conn) {
            conn.query("select * from major", function(err, major) {
              conn.query("select * from department", function(err, department) {
                if (err) {
                  req.session.errors = errors;
                  req.session.success = false;
                  res.redirect('/adminstudent/add')
                } else {
                    res.render("adminstudent/adminstudentAdd", {
                        data1: major,
                        data2: department ,
                        session: req.session
                    });
                };
              });
            });
        });
      } else {
          res.redirect('/');
      };
};
module.exports = controller;
