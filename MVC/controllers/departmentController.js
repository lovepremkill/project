const controller = {};

controller.list = function(req, res) {

        if (req.session.admin) {
            req.getConnection(function(err, conn) {
                conn.query("select * from department order by department.id asc", function(err, department) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.render("department/departmentList", {
                            data: department,
                            session: req.session
                        });
                    };
                });
            });

    } else {
        res.redirect('/');
    };
};

module.exports = controller;
