const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
  if(req.session.admin){
    req.getConnection((err, conn) => {
        conn.query('select * FROM teacher;', (err, scitproject) => {
            if (err) {
                res.json(err);
            } else {
                res.render('uiadminteacher/uiadminteacherList', {
                    session: req.session,
                    data: scitproject
                });
            }


        });
    });
  }else{
res.rediract('/');
};
};


// controller.add = (req,res) => {
//   const { id } = req.params;
//   req.getConnection( (err, conn) => {
//     conn.query('select * FROM teacher;', [id], (err, uiadminteacher) => {
//
//       if ( err ) {
//         res.json(err)
//       } else {
//          res.render('uiadminteacher/uiadminteacherAdd', {
//            session: req.session,
//            data: uiadminteacher[0]
//          });
//       }
//     });
//   });
//   res.render('uiadminteacher/uiadminteacherAdd', {
//   });
// };

controller.add = (req, res) => {
  if(req.session.admin){
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * FROM teacher;', [id], (err, uiadminteacher) => {
            if (err) {
                res.json(err)
            } else {
                res.render('uiadminteacher/uiadminteacherAdd', {
                    session: req.session,
                    data: uiadminteacher
                });
            }
        });
    });
  }else{
res.rediract('/');
};

};
//
// controller.search = (req,res) => {
//       const data = req.body;
//
//       req.getConnection( (err, conn) => {
//         conn.query('select * FROM scitproject;', (err, oil) => {
//           if ( err ) {
//             res.json(err)
//           } else {
//
//             if (data.member_id != ""){
//
//               req.getConnection( (err, conn) => {
//                 conn.query('select * FROM scitproject;',[data.member_id], (err, uiadminteacher) => {
//                   if ( err ) {
//                     res.json(err)
//                   } else {
//                      res.render('uiadminteacherAdd', {
//                        session: req.session,
//                        error: false,
//                        data: uiadminteacher[0],
//                        oil:oil
//                      });
//                   }
//                 });
//               });
//
//             } else if(data.member_name != ""){
//
//               req.getConnection( (err, conn) => {
//                 conn.query('select * FROM scitproject;',[data.member_name], (err, uiadminteacher) => {
//                   if ( err ) {
//                     res.json(err)
//                   } else {
//                      res.render('uiadminteacherAdd', {
//                        session: req.session,
//                        error: false,
//                        data: uiadminteacher[0],
//                        oil:oil
//                      });
//                   }
//                 });
//               });
//
//             } else if(data.member_id == "" & data.member_name == ""){
//
//               res.render('uiadminteacherAdd', {
//                 session: req.session,
//                 error: true,
//                 data: [],
//                 oil:oil
//               });
//
//             }
//
//           }
//         });
//       });
// };
//
controller.save = (req, res) => {
  if(req.session.admin){
    const data = req.body;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/uiadminteacher/add');
    } else {
        req.session.success = true;
        req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
        req.getConnection((err, conn) => {
            conn.query('insert into teacher set ?', [data], (err, uiadminteacher) => {
                if (err) {
                    res.json(err)
                } else {
                    res.redirect('/uiadminteacher');
                }
            });
        });
    }
  }else{
res.rediract('/');
};

};
//
controller.edit = (req, res) => {
    if(req.session.admin){
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * FROM teacher  WHERE id= ?', [id], (err, teacher) => {
            console.log(teacher)
            if (err) {
                res.json(err)
            } else {
                res.render('uiadminteacher/uiadminteacherEdit', {
                    session: req.session,
                    data: teacher[0],
                });
            }
        });
    });
  }else{
res.rediract('/');
};
};

controller.update = (req, res) => {
  if(req.session.admin){
    const data = req.body;
    const { id } = req.params;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;

        req.getConnection((err, conn) => {
            conn.query('select * FROM teacher  WHERE id= ?', [id], (err, teacher) => {
                console.log(teacher)
                if (err) {
                    res.json(err)
                } else {
                    res.render('uiadminteacher/uiadminteacherEdit', {
                        session: req.session,
                        data: teacher[0],
                    });
                }
            });
        });

    } else {
        req.session.success = true;
        req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
        req.getConnection((err, conn) => {
            conn.query('update teacher set ? WHERE id= ?', [data, id], (err, uiadminteacher) => {
                if (err) {
                    res.json(err)
                } else {
                    res.redirect('/uiadminteacher');
                }
            });
        });
    }
  }else{
res.rediract('/');
};

};
//

controller.del = (req, res) => {
  if(req.session.admin){
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * FROM teacher where id = ?', [id], (err, teacher) => {
            console.log(teacher)
            if (err) {
                res.json(err);
            }
            res.render('uiadminteacher/uiadminteacherDelete', { session: req.session, data: teacher[0] });
        });
    });
  }else{
res.rediract('/');
};
};

controller.delete = (req, res) => {
  if(req.session.admin){
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM teacher where id = ?', [id], (err, uiadminteacher) => {
            if (err) {
                res.json(err)
                const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
                req.session.errors = errorss;
                req.session.success = false;
            } else {
                req.session.success = true;
                req.session.topic = "ลบข้อมูลเสร็จแล้ว";
                res.redirect('/uiadminteacher');
            }
        });
    });
  }else{
res.rediract('/');
};
};


module.exports = controller;
