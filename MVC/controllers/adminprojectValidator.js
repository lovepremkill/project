const { check } = require('express-validator');

exports.addValidator = [check('nameth',"โปรดใส่ชื่อวิจัย(ไทย)").not().isEmpty(),
                        check('nameen',"โปรดใส่ชื่อวิจัย(อังกฤษ)").not().isEmpty(),
                        check('teacher_id',"โปรดเลือกอาจารย์ที่ปรึกษา").not().isEmpty(),
                        check('year',"โปรดใส่ปีการศึกษา").not().isEmpty(),
                        check('year',"โปรดใส่ปีการศึกษาเป็นตัวเลข").isInt()
                      ];

exports.editValidator = [check('nameth',"โปรดใส่ชื่อวิจัย(ไทย)").not().isEmpty(),
                        check('nameen',"โปรดใส่ชื่อวิจัย(อังกฤษ)").not().isEmpty(),
                        check('teacher_id',"โปรดเลือกอาจารย์ที่ปรึกษา").not().isEmpty(),
                        check('year',"โปรดใส่ปีการศึกษา").not().isEmpty(),
                        check('year',"โปรดใส่ปีการศึกษาเป็นตัวเลข").isInt()
                      ];
exports.addStudentValidator1 = [check('department_id',"โปรดเลือก").not().isEmpty()
                       ];
exports.addStudentValidator2 = [check('major_id',"โปรดเลือก").not().isEmpty()
                       ];
 exports.addStudentValidator3 = [check('student_id',"โปรดเลือก").not().isEmpty()
                ];
