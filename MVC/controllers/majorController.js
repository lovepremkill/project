const controller = {};

controller.list = function(req, res) {

        if (req.session.admin) {
            req.getConnection(function(err, conn) {
                conn.query("select major.id idmajor, major.name man, major.department_id, department.id idde, department.name den from major join department on major.department_id = department.id ORDER BY major.department_id asc;", function(err, major) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.render("major/majorList", {
                            data: major,
                            session: req.session
                        });
                    };
                });
            });

    } else {
        res.redirect('/');
    };
};

module.exports = controller;
