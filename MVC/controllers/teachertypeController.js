const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
    if(req.session.admin || req.session.teacher){
req.getConnection((err, conn) => {
        conn.query('select * from teachertype;', (err, teachertype) => {
            if (err) {
                res.json(err);
            }else {
            res.render('teachertype/teachertypeList', { session: req.session, data: teachertype });
          }
        });
    });
  } else {
      res.redirect('/');
  };
};

controller.save = (req, res) => {
  if(req.session.admin || req.session.teacher){
    const data = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/teachertype/add');
    } else {
        req.session.success = true;
        req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO teachertype set ?', [data], (err, teachertype) => {
                res.redirect('/teachertype');
            });
        });
    };
  } else {
      res.redirect('/');
  };

};

controller.del = (req, res) => {
if(req.session.admin || req.session.teacher){
    const { id } = req.params;
req.getConnection((err, conn) => {
        conn.query('select * from teachertype HAVING id = ?', [id], (err, teachertype) => {
            if (err) {
                res.json(err);
            }else {
            res.render('teachertype/teachertypeDelete', { session: req.session, data: teachertype[0] });
          }
        });
    });

  } else {
      res.redirect('/');
  };

};

controller.delete = (req, res) => {
  if(req.session.admin || req.session.teacher){
    const { id } = req.params;

    req.getConnection((err, conn) => {
        conn.query('DELETE FROM teachertype WHERE id= ?', [id], (err, teachertype) => {
            if (err) {
                const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
                req.session.errors = errorss;
                req.session.success = false;
            } else {
                req.session.success = true;
                req.session.topic = "ลบข้อมูลเสร็จแล้ว";
            }
            console.log(teachertype);
            res.redirect('/teachertype');
        });
    });
  } else {
      res.redirect('/');
  };
};

controller.edit = (req, res) => {
  if(req.session.admin || req.session.teacher){
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM teachertype WHERE id= ?', [id], (err, teachertype) => {
          if (err) {
              res.json(err);
          }else {
            res.render('teachertype/teachertypeUpdate', {
                session: req.session,
                data1: teachertype[0]
            });
          }
        });
    });
  } else {
      res.redirect('/');
  };
}
controller.update = (req, res) => {
  if(req.session.admin || req.session.teacher){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;

        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM teachertype WHERE id= ?', [id], (err, teachertype) => {
              if (err) {
                  res.json(err);
              }else {
                res.render('teachertype/teachertypeUpdate', {
                    session: req.session,
                    data1: teachertype[0]
                });
              }
            });
        });

    } else {
        req.session.success = true;
        req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
        req.getConnection((err, conn) => {
            conn.query('UPDATE  teachertype SET ?  WHERE id = ?', [data, id], (err, teachertype) => {
                if (err) {
                    res.json(err);
                }
                res.redirect('/teachertype');
            });
        });
    }
  } else {
      res.redirect('/');
  };
};

controller.add = (req, res) => {
  if(req.session.admin || req.session.teacher){
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM teachertype', (err, ce611998018) => {
          if (err) {
              res.json(err);
          }else {
            res.render('teachertype/teachertypeAdd', { data: ce611998018, session: req.session });
          }
        });
    });
  } else {
      res.redirect('/');
  };
};
module.exports = controller;
