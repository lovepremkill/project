
const controller = {};
const { json } = require("body-parser");
const { LOADIPHLPAPI } = require("dns");
const uuidv4 = require('uuid').v4;
const { validationResult } = require('express-validator');


controller.list = (req,res) => {
  if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
if(req.session.admin){
  req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM project order by year desc',(err,project)=>{
      conn.query('SELECT pj.id as pjid,pj.nameen as pjnameen,pj.nameth as pjnameth,st.firstname as stfirstname,st.lastname as stlastname,tc.name as tcname,mj.name as mjname,pj.year as pjyear,stpj.project_id as project_id FROM project as pj  left join studentproject as stpj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id',(err,adminproject) =>{
          if(err){
              res.json(err);
          }
          res.render('adminproject/adminprojectList',{
            session: req.session,
            data1:adminproject,
            data:project,admin:admin1,teacher:teacher1,student1:student1
          });
          });
      });
  });

}else{
res.redirect('/');
};
    };
  }


controller.save = (req,res) => {
    if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const data=req.body;
  const errors = validationResult(req);
  if(req.session.admin){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/adminproject/add');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";

          req.getConnection((err,conn) =>{
            conn.query('INSERT INTO project  set ?',[data],(err,project) =>{
              if(err){
                res.json(err);
              }
              console.log(project);
              res.redirect('/adminproject');
            });
          });
    }

  }else{
res.redirect('/');
};

};
}

controller.savestudent = (req,res) => {
    if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const { id } = req.params;
    const data=req.body;
    if(req.session.admin){
    if(data.student_id==""){
      const  errorss= {errors: [{ value: '', msg: 'โปรดเลือกผู้วิจัยใหม่ ', param: '', location: '' }]}
    req.session.errors=errorss;
    req.session.success=false;
        req.getConnection((err,conn) => {
          conn.query('SELECT * FROM department ',(err,department)=>{
                res.render('adminproject/adminprojectAddStudent',{iddata:id,data1:department,session: req.session,admin:admin1,teacher:teacher1,student1:student1});
          });
      });
  }else {
    console.log(data);
      if(data.teacher_id==""){data.teacher_id = null;}
      const errors = validationResult(req);
          if(!errors.isEmpty()){
              req.session.errors=errors;
              req.session.success=false;
              res.redirect('/adminproject');
          }else{
          //    req.session.success=true;
            //  req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                  conn.query('INSERT INTO studentproject set student_id = ?,project_id= ?',[data.student_id,id],(err,project)=>{
                    conn.query('SELECT pj.id as pjid,pj.nameen as pjnameen,pj.nameth as pjnameth,st.firstname as stname,tc.name as tcname,mj.name as mjname,pj.year as pjyear,pj.abstract as pjabstract,st.phone as stphone, st.studentcode as ststudentcode FROM project as pj left join studentproject as stpj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?;',[id],(err,adminproject) =>{
                      conn.query('SELECT pj.id as pjid,st.id as stid, st.firstname as stfirstname,st.lastname as stlastname,st.phone as stphone,st.studentcode as ststudentcode FROM studentproject as stpj left join project as pj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?',[id],(err,adminproject1) =>{
                        if(err){
                          res.json(err);
                        }
                        res.render('adminproject/adminprojectReport',{
                          data:adminproject[0],
                          data2:adminproject1,
                          session: req.session,admin:admin1,teacher:teacher1,student1:student1
                        });
                  });
                  });

                  });
              });
          }
  }

}else{
res.redirect('/');
};

};
}

controller.delete =(req,res) =>{
    if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const {id} =req.params;
if(req.session.admin){
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM project WHERE id = ?',[id],(err,tb) =>{
          if(err){
            const  errorss= {errors: [{ value: '', msg: 'โปรดลบผู้วิจัยและไฟล์ก่อนลบงานวิจัย', param: '', location: '' }]}
            req.session.errors=errorss;
            req.session.success=false;
            conn.query('SELECT pj.id as pjid,pj.nameen as pjnameen,pj.nameth as pjnameth,st.firstname as stname,tc.name as tcname,mj.name as mjname,pj.year as pjyear,pj.abstract as pjabstract,st.phone as stphone, st.studentcode as ststudentcode FROM project as pj left join studentproject as stpj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?;',[id],(err,adminproject) =>{
              conn.query('SELECT pj.id as pjid,st.id as stid, st.firstname as stfirstname,st.lastname as stlastname,st.phone as stphone,st.studentcode as ststudentcode FROM studentproject as stpj left join project as pj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?',[id],(err,adminproject1) =>{
                if(err){
                  res.json(err);
                }
                res.render('adminproject/adminprojectReport',{
                  data:adminproject[0],
                  data2:adminproject1,
                  session: req.session,admin:admin1,teacher:teacher1,student1:student1
                });
          });
          });
          }else {
            req.session.success=true;
            req.session.topic="ลบข้อมูลเสร็จแล้ว";
            console.log(tb);
            res.redirect('/adminproject');
          }
        });
      });

    }else{
res.redirect('/');
};

};
}

controller.deletestudent = (req,res) => {
    if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
    const { id } = req.params;
    const sid =id.split("_")[0];
    const pid =id.split("_")[1];
    if(req.session.admin){
    const  errorss= {errors: [{ value: '', msg: 'ลบข้อมูลนี้ไม่ได้ ', param: '', location: '' }]}
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM studentproject WHERE project_id = ? && student_id = ?',[pid,sid],(err,problemreport)=>{
            if(err){
                req.session.errors=errorss;
                req.session.success=false;
            }else{
            req.session.success=true;
            req.session.topic="ลบข้อมูลเสร็จแล้ว";
            conn.query('SELECT pj.id as pjid,pj.nameen as pjnameen,pj.nameth as pjnameth,st.firstname as stname,tc.name as tcname,mj.name as mjname,pj.year as pjyear,pj.abstract as pjabstract,st.phone as stphone, st.studentcode as ststudentcode FROM project as pj left join studentproject as stpj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?;',[pid],(err,adminproject) =>{
              conn.query('SELECT pj.id as pjid,st.id as stid, st.firstname as stfirstname,st.lastname as stlastname,st.phone as stphone,st.studentcode as ststudentcode FROM studentproject as stpj left join project as pj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?',[pid],(err,adminproject1) =>{
                if(err){
                  res.json(err);
                }
                res.render('adminproject/adminprojectReport',{
                  data:adminproject[0],
                  data2:adminproject1,
                  session: req.session,admin:admin1,teacher:teacher1,student1:student1
                });
          });
          });
          }
        });
    });

  }else{
res.redirect('/');
};

};
}

controller.del =(req,res) =>{
  if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const {id} =req.params;
  if(req.session.admin){
      req.getConnection((err,conn) =>{
        conn.query('SELECT pj.id as pjid,pj.nameen as pjnameen,pj.nameth as pjnameth,st.firstname as stname,tc.name as tcname,mj.name as mjname,pj.year as pjyear,pj.abstract as pjabstract,st.phone as stphone, st.studentcode as ststudentcode FROM project as pj left join studentproject as stpj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?;',[id],(err,adminproject) =>{
          conn.query('SELECT pj.id as pjid, st.firstname as stfirstname,st.lastname as stlastname,st.phone as stphone,st.studentcode as ststudentcode FROM studentproject as stpj left join project as pj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?',[id],(err,adminproject1) =>{
            if(err){
              res.json(err);
            }
            res.render('adminproject/adminprojectDelete',{
              data:adminproject[0],
              data2:adminproject1,
              session: req.session,admin:admin1,teacher:teacher1,student1:student1
            });
      });
      });
      });

    }else{
    res.redirect('/');
    };

};
}

controller.delstudent = (req,res) => {
    if(req.session.admin){
  const { id } = req.params;
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
console.log(id);
const sid =id.split("_")[0];
const pid =id.split("_")[1];
console.log(pid);
console.log(sid);
if(req.session.admin){
  req.getConnection((err,conn)=>{
              conn.query('SELECT * FROM project WHERE ID=?',[pid],(err,project)=>{
                  conn.query('SELECT * FROM student WHERE ID=?',[sid],(err,student)=>{
                res.render('adminproject/adminprojectStudentDelete',{data1:project,data2:student,data3:id,data4:pid,session: req.session,admin:admin1,teacher:teacher1,student1:student1});
              });
              });
          });

        }else{
        res.redirect('/');
        };

}
}


controller.edit =(req,res) =>{
    if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const {id} =req.params;
if(req.session.admin){
  req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM project where id = ?',[id],(err,pj) => {
        conn.query('SELECT * FROM teacher',(err,tc) => {
        res.render('adminproject/adminprojectUpdate',{
          data:pj[0],
          data2:tc,
          data3:id,
          session: req.session,admin:admin1,teacher:teacher1,student1:student1
        });
      })
    });
    });

  }else{
res.redirect('/');
};

};
}


controller.update = (req,res) => {
    if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
if(req.session.admin){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM project where id = ?',[id],(err,pj) => {
            conn.query('SELECT * FROM teacher',(err,tc) => {
            res.render('adminproject/adminprojectUpdate',{
              data:pj[0],
              data2:tc,
              data3:id,
              session: req.session,admin:admin1,teacher:teacher1,student1:student1
            });
          })
        });
        });

    }else {
      req.session.success=true;
      req.session.topic="แก้ไข้ข้อมูลสำเร็จ";
          req.getConnection((err,conn) =>{
            conn.query('UPDATE project SET ? WHERE id = ?',[data,id],(err,tb) =>{
              if(err){
                res.json(err);
              }
              conn.query('SELECT pj.id as pjid,pj.nameen as pjnameen,pj.nameth as pjnameth,st.firstname as stname,tc.name as tcname,mj.name as mjname,pj.year as pjyear,pj.abstract as pjabstract,st.phone as stphone, st.studentcode as ststudentcode FROM project as pj left join studentproject as stpj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?;',[id],(err,adminproject) =>{
                conn.query('SELECT pj.id as pjid,st.id as stid, st.firstname as stfirstname,st.lastname as stlastname,st.phone as stphone,st.studentcode as ststudentcode FROM studentproject as stpj left join project as pj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?',[id],(err,adminproject1) =>{
                  if(err){
                    res.json(err);
                  }
                  res.render('adminproject/adminprojectReport',{
                    data:adminproject[0],
                    data2:adminproject1,
                    session: req.session,admin:admin1,teacher:teacher1,student1:student1
                  });
            });
            });
            });
          });
    }

  }else{
res.redirect('/');
};

};
}


controller.add =(req,res) =>{
    if(req.session.admin){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const {id} =req.params;
  if(req.session.admin){
      req.getConnection((err,conn) =>{
            conn.query('SELECT * FROM teacher',[id],(err,tc) => {
            res.render('adminproject/adminprojectAdd',{
              data:null,
              data2:tc,
              session: req.session,admin:admin1,teacher:teacher1,student1:student1
            });
          })
        });

      }else{
res.redirect('/');
};

      };
    }


      controller.addstudent = (req,res) => {
        if(req.session.admin){
      const { id } = req.params;
      const admin1 =req.session.admin;
      const teacher1 =req.session.teacher;
      const student1 =req.session.student;
      if(req.session.admin){
          req.getConnection((err,conn) => {
            conn.query('SELECT * FROM department ',(err,department)=>{
                  res.render('adminproject/adminprojectAddStudent',{iddata:id,data1:department,session: req.session,admin:admin1,teacher:teacher1,student1:student1});
            });
        });

      }else{
res.redirect('/');
};

      };
    }

      controller.addstudent2 = (req,res) => {
          if(req.session.admin){
        const admin1 =req.session.admin;
        const teacher1 =req.session.teacher;
        const student1 =req.session.student;
        const data=req.body;
        const { id } = req.params;
      console.log(data);
      if(req.session.admin){
      if(data.department_id==""){
        const  errorss= {errors: [{ value: '', msg: 'โปรดเลือกสำนัก ', param: '', location: '' }]}
      req.session.errors=errorss;
      req.session.success=false;
          req.getConnection((err,conn) => {
            conn.query('SELECT * FROM department ',(err,department)=>{
                  res.render('adminproject/adminprojectAddStudent',{iddata:id,data1:department,session: req.session,admin:admin1,teacher:teacher1,student1:student1});
            });
        });
  }else {
    req.getConnection((err,conn) => {
    conn.query('SELECT * FROM department ',(err,department)=>{
      conn.query('SELECT * FROM department where id =? ',[data.department_id],(err,department2)=>{
      conn.query('SELECT * FROM major where department_id=? ',[data.department_id],(err,major)=>{
          res.render('adminproject/adminprojectAddStudent2',{session: req.session,admin:admin1,teacher:teacher1,student1:student1,iddata:id,data:data,data1:department,data2:major,data3:department2});

    });});});
    });
    };

  }else{
res.redirect('/');
};

  }
}


      controller.addstudent3 = (req,res) => {
          if(req.session.admin){
        const admin1 =req.session.admin;
        const teacher1 =req.session.teacher;
        const student1 =req.session.student;
        const data=req.body;
        const { id } = req.params;
      console.log(data);
      if(req.session.admin){
    if(data.major_id==""){
      req.getConnection((err,conn) => {
    const  errorss= {errors: [{ value: '', msg: 'โปรดเลือกสาขา ', param: '', location: '' }]}
    req.session.errors=errorss;
    req.session.success=false;
    conn.query('SELECT * FROM department ',(err,department)=>{
      conn.query('SELECT * FROM department where id =? ',[data.department_id],(err,department2)=>{
      conn.query('SELECT * FROM major where department_id=? ',[data.department_id],(err,major)=>{
          res.render('adminproject/adminprojectAddStudent2',{session: req.session,admin:admin1,teacher:teacher1,student1:student1,iddata:id,data:data,data1:department,data2:major,data3:department2});

    });});});
  });}else {
    req.getConnection((err,conn) => {
        conn.query('SELECT * FROM department ',(err,department)=>{
          conn.query('SELECT * FROM department where id = ? ',[data.department_id],(err,department2)=>{
        conn.query('SELECT * FROM major where department_id=? ',[data.department_id],(err,major)=>{
            conn.query('SELECT * FROM major  where id = ? ',[data.major_id],(err,major2)=>{
          conn.query('SELECT * FROM student where major_id=? ',[data.major_id],(err,student)=>{
              res.render('adminproject/adminprojectAddStudent3',{iddata:id,data:data,data1:department,data2:major,data3:student,data4:department2,data5:major2,session: req.session,admin:admin1,teacher:teacher1,student1:student1});

        });});});});});
    });
  }

}else{
res.redirect('/');

};
      };
    }

      controller.report = (req,res) => {
          if(req.session.admin){
        const admin1 =req.session.admin;
        const teacher1 =req.session.teacher;
        const student1 =req.session.student;
          const { id } = req.params;
          if(req.session.admin){
          req.getConnection((err,conn) => {
              conn.query('SELECT pj.id as pjid,pj.nameen as pjnameen,pj.nameth as pjnameth,st.firstname as stname,tc.name as tcname,mj.name as mjname,pj.year as pjyear,pj.abstract as pjabstract,st.phone as stphone, st.studentcode as ststudentcode FROM project as pj left join studentproject as stpj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?;',[id],(err,adminproject) =>{
                conn.query('SELECT pj.id as pjid,st.id as stid, st.firstname as stfirstname,st.lastname as stlastname,st.phone as stphone,st.studentcode as ststudentcode FROM studentproject as stpj left join project as pj on stpj.project_id = pj.id left join student as st on stpj.student_id = st.id left join major as mj on st.major_id = mj.id left join teacher as tc on pj.teacher_id =tc.id  HAVING pjid = ?',[id],(err,adminproject1) =>{
                  if(err){
                    res.json(err);
                  }
                  res.render('adminproject/adminprojectReport',{
                    data:adminproject[0],
                    data2:adminproject1,
                    session: req.session,admin:admin1,teacher:teacher1,student1:student1
                  });
            });
            });
          });
        }else{
res.redirect('/');
};

        };
      }

        controller.upload = (req, res) => {
            if(req.session.admin){
          const { id } = req.params;
          if(req.session.admin){
          req.getConnection((err, conn) => {
            conn.query('select * from project where id = ?;', [id], (err, upload) => {
              res.render('adminproject/adminprojectUpload', {
                data: id, session: req.session, data1: upload[0]
              });
            });
          });

        }else{
res.redirect('/');
};

        };
      }

        controller.uploadUp = (req, res) => {
          if(req.session.admin){
          const { id } = req.params;
          const { key } = req.params;
          const data = req.files;
if(req.session.admin){
          if (req.files) {
            var file = req.files.filename;
            if (!Array.isArray(file)) {
              var filename = uuidv4() + "." + file.name.split(".")[1];
              file.mv("./public/listProject/" + filename, function (err) {
                if (err) { console.log(err); }
              })
            } else {
              for (var i = 0; i < file.length; i++) {
                var filename = uuidv4() + "." + file[i].name.split(".")[1];
                file[i].mv("./public/listProject/" + filename, function (err) {
                  if (err) { console.log(err); }
                })
              }
            }
          }
          req.getConnection((err, conn) => {
            if (key == 1) {
              conn.query('UPDATE project set coverout = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 2) {
              conn.query('UPDATE project set coverin = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 3) {
              conn.query('UPDATE project set certificate = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 4) {
              conn.query('UPDATE project set abstract = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 5) {
              conn.query('UPDATE project set acknowledgment = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 6) {
              conn.query('UPDATE project set contents = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 7) {
              conn.query('UPDATE project set chapter1 = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 8) {
              conn.query('UPDATE project set chapter2 = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 9) {
              conn.query('UPDATE project set chapter3 = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 10) {
              conn.query('UPDATE project set chapter4 = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 11) {
              conn.query('UPDATE project set chapter5 = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 12) {
              conn.query('UPDATE project set refreces = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 13) {
              conn.query('UPDATE project set appendimage = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
            if (key == 14) {
              conn.query('UPDATE project set biography = ? where id = ?', [filename, id], (err, upload) => {
                res.redirect('/adminproject/upload/' + id);
              });
            }
          });

        }else{
res.redirect('/');
};
};
}

module.exports = controller;
