const controller = {};
const { fail } = require('assert');
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');


controller.log = function(req, res) {
  req.getConnection((err, conn) => {
      conn.query('select   st.lastname as lastname,st.studentcode as studentcode,st.id as idst,pj.id as idpj,mj.id as idmj,pj.nameen as engname,pj.nameth as thname,st.firstname as fname,st.lastname as lname,mj.name as mjname,pj.year as yearmj,t.name as tname from studentproject as spro left join project as pj on spro.project_id=pj.id left join  student as st on spro.student_id=st.id left join major as mj on st.major_id=mj.id left join teacher as t on pj.teacher_id=t.id ;',
          (err, uilist) => {
              conn.query('select * from major ;', (err2, major) => {
                  if (err) {
                      res.json(err);
                  }
                  res.render('uilist/uiList', {
                      session: req.session,
                      data2: major,
                      data: uilist
                  });
              })
          });
  });
};


controller.loginadmin = function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/');
    } else {
        req.getConnection(function(err, conn) {
            conn.query('SELECT * FROM admin WHERE username = ? AND password = ?', [req.body.username, req.body.password], function(err, data1) {
                if (err) {
                    res.json(err);
                } else {
                    if (data1.length > 0) {
                        req.session.admin = data1[0].id;
                        res.redirect('/adminproject');
                    } else {
                        req.session.errors = errors;
                        req.session.success = true;
                        req.session.topic = "ชื่อผู้ใช้ หรือ รหัสผ่าน ไม่ถูกต้อง"
                        res.redirect('/');
                    }
                };
            });
        });
    };
};
controller.loginteacher = function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/');
    } else {
        req.getConnection(function(err, conn) {
            conn.query('SELECT * FROM teacher WHERE username = ? AND password = ?', [req.body.username, req.body.password], function(err, data2) {
                if (err) {
                    res.json(err);
                } else {
                    if (data2.length > 0) {
                        req.session.teacher = data2[0].id;
                        res.redirect('/uiteacher');
                    } else {
                        req.session.errors = errors;
                        req.session.success = true;
                        req.session.topic = "ชื่อผู้ใช้ หรือ รหัสผ่าน ไม่ถูกต้อง"
                        res.redirect('/');
                    }
                };
            });
        });
    };
};
controller.loginstudent = function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/');
    } else {
        req.getConnection(function(err, conn) {
            conn.query('SELECT * FROM student WHERE studentcode = ? AND password = ?', [req.body.studentcode, req.body.password], function(err, data3) {
                if (err) {
                    res.json(err);
                } else {
                    if (data3.length > 0) {
                        req.session.student = data3[0].id;
                        res.redirect('/uistudent');
                    } else {
                        req.session.errors = errors;
                        req.session.success = true;
                        req.session.topic = "ชื่อผู้ใช้ หรือ รหัสผ่าน ไม่ถูกต้อง"
                        res.redirect('/');
                    }
                };
            });
        });
    };
};


controller.logout = function(req, res) {
    req.session.destroy();
    res.redirect('/');
};

controller.home = function(req, res) {
    if (req.session.admin || req.session.teacher || req.session.student) {
        res.redirect('/home');
    } else {
        res.redirect('/');
    };
};
module.exports = controller;
