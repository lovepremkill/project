
const { check } = require('express-validator');

exports.addValidator = [check('firstname',"ชื่อไม่ถูกต้อง").not().isEmpty(),

                        check('lastname',"นามสุกลไม่ถูกต้อง").not().isEmpty(),

                        check('phone',"เบอร์เบอร์โทรศัพท์ไม่ถูกต้อง").isInt(),

                        check('studentcode',"รหัสนักศึกษาไม่ถูดต้อง").isInt(),

                        check('password',"รหัสไม่ถูกต้อง").not().isEmpty(),

                        check('major_id',"คณะไม่ถูกต้อง").not().isEmpty(),

                        check('birthday',"วันเดือนปีเกิดไม่ถูกต้อง").not().isEmpty(),

                        check('address',"ที่อยู่ไม่ถูกต้อง").not().isEmpty(),

                        check('email',"อีเมลไม่ถูกต้อง").not().isEmpty()
                      ];
