const { check } = require('express-validator');
exports.loginadmin = [
    check('username', "ชื่อผู้ใช้ไม่ถูกต้อง").not().isEmpty(),
    check('password', "รหัสผ่านไม่ถูกต้อง").not().isEmpty()
];
exports.loginteacher = [
    check('username', "ชื่อผู้ใช้ไม่ถูกต้อง").not().isEmpty(),
    check('password', "รหัสผ่านไม่ถูกต้อง").not().isEmpty()
];
exports.loginastudent = [
    check('studentcode', "ชื่อผู้ใช้ไม่ถูกต้อง").not().isEmpty(),
    check('password', "รหัสผ่านไม่ถูกต้อง").not().isEmpty()
];