const controller = {};
const { validationResult } = require('express-validator');

controller.list = function(req, res) {

        if (req.session.admin) {
            req.getConnection(function(err, conn) {
                conn.query("select * from department order by department.id asc", function(err, department) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.render("adminDepartment/adminDepartmentList", {
                            data: department,
                            session: req.session
                        });
                    };
                });
            });

    } else {
        res.redirect('/');
    };
};

controller.add = function(req, res) {

        if (req.session.admin) {
            req.getConnection(function(err, conn) {
                conn.query("select * from department ", function(err, department) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.render("adminDepartment/adminDepartmentAdd", {
                            data: department,
                            session: req.session
                        });
                    };
                });
            });

    } else {
        res.redirect('/');
    };
};

controller.save = function(req, res) {

        if (req.session.admin) {
            const data = req.body;
            const errors = validationResult(req);
            if (data.department_id == "") { data.department_id = null };
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                res.redirect('/adminDepartment/add');
            } else {
                req.session.success = true;
                req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
                req.getConnection(function(err, conn) {
                    conn.query("insert into department set ?", [data], function(err, department) {
                        if (err) {
                            console.log(err);
                        } else {
                            res.redirect('/adminDepartment');
                        };
                    });
                });
            };

    } else {
        res.redirect('/');
    };
};

controller.del = (req, res) => {

        if (req.session.admin) {
            const { id } = req.params;
            req.getConnection((err, conn) => {
                conn.query("select * from department where department.id= ?", [id], function(err, department) {
                        if (err) {
                            console.log(err);
                        }
                        res.render('adminDepartment/adminDepartmentDelete', {
                            session: req.session,
                            data1: department[0],
                        });
                    });
                });

    } else {
        res.redirect('/');
    };
};

controller.delete = function(req, res) {

        if (req.session.admin) {
            const { id } = req.params;
            const errorss = { errors: [{ value: '', msg: 'ลบข้อมูลนี้ไม่ได้ ', param: '', location: '' }] }
            req.getConnection(function(err, conn) {
                conn.query("delete from department where id = ?", [id], function(err, department) {
                    if (err) {
                        console.log(err);
                        req.session.errors = errorss;
                        req.session.success = false;
                    } else {
                        req.session.success = true;
                        req.session.topic = "ลบข้อมูลเสร็จแล้ว";
                    };
                    res.redirect('/adminDepartment');
                });
            });
    } else {
        res.redirect('/');
    };
};

controller.edit = function(req, res) {

        if (req.session.admin) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query("select * from department where department.id = ?", [id], function(err, department) {
                        if (err) {
                            console.log(err);
                        } else {
                            res.render("adminDepartment/adminDepartmentUpdate", {
                                data: department[0],
                                session: req.session
                            });
                        };
                    });
                });

    } else {
        res.redirect('/');
    };
};

controller.update = function(req, res) {

        if (req.session.admin) {
            const { id } = req.params;
            const data = req.body;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                req.getConnection((err, conn) => {
                    conn.query('SELECT * FROM major WHERE id= ?', [id], function(err, major) {
                        conn.query('SELECT * FROM department', [id], function(err, department) {
                            if (err) {
                                console.log(err);
                            }
                            res.render('adminDepartment/adminDepartmentUpdate', {
                                session: req.session,
                                data: major[0],
                                data2: department

                            });
                        });
                    });
                });
            } else {
                req.session.success = true;
                req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
                req.getConnection(function(err, conn) {
                    conn.query('update department set ? where id = ?', [data, id], function(err, major) {
                        if (err) {
                            console.log(err);
                        }
                        res.redirect('/adminDepartment')
                    });
                });
            };

    } else {
        res.redirect('/');
    };
};
module.exports = controller;
