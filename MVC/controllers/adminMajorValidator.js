const { check } = require('express-validator');
exports.addValidator = [
    check('name', "กรุณาใส่ชื่อโปรแกรมวิชา").not().isEmpty(),
    check('department_id', "กรุณาใส่เลือกชื่อคณะ").not().isEmpty(),
];

exports.updateValidator = [
    check('name', "กรุณาใส่ชื่อโปรแกรมวิชา").not().isEmpty(),
    check('department_id', "กรุณาใส่เลือกชื่อคณะ").not().isEmpty(),
];