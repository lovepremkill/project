const controller = {};
const e = require('express');

controller.list = function (req, res) {
    const { idproj } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select pro.nameen as pronameen,pro.nameth as pronameth,pro.year as proyear,t.name as tname,t.jobposition as tjobposition,t.email as temail from project as pro left JOIN teacher as t ON pro.teacher_id = t.id where pro.id = ? ;', [idproj], (err, pro) => {
            conn.query('select st.firstname as stfirstname,st.lastname as stlastname,st.phone as stphone,st.studentcode as ststudentcode,DATE_FORMAT(st.birthday,"%d-%m-%Y") as stbirthday,st.address as staddress ,st.email as stemail,m.name as mname,d.name as dname from studentproject as stj JOIN project as pro ON stj.project_id = pro.id JOIN student as st ON stj.student_id = st.id JOIN major as m ON st.major_id = m.id JOIN department as d ON m.department_id=d.id where pro.id=?', [idproj], (err2, stu) => {
                conn.query('select t.name as tname,t.email as temail ,tty.name as ttname from teacherproject as tpr join teacher as t on tpr.teacher_id = t.id join project as pro on tpr.project_id = pro.id join teachertype as tty on tpr.teachertype_id=tty.id  where pro.id=?', [idproj], (err3, tty) => {
                    conn.query('select * from project where id = ?', [idproj], (err4, porj) => {
                        if (err) {
                            res.redirect('/');
                        } else if (err2) {
                            res.redirect('/');
                        } else if (err3) {
                            res.redirect('/');
                        } else if (err4) {
                            res.redirect('/');
                        } else {
                            res.render('uiuserdetail/uiuserdetailList', {
                                data: pro[0],
                                data2: stu,
                                data3: tty,
                                data4: porj[0],
                                session: req.session
                            });
                        }
                    })
                })
            });
        });
    });
};

module.exports = controller;
