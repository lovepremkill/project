const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  if(req.session.admin){
    req.getConnection((err,conn) =>{
        conn.query('select * from admin;',(err,admintype) =>{
            if(err){
                res.json(err);
            }
            res.render('admin/adminList',{session: req.session,
              data:admintype,
              });
            });
        });
      } else {
          res.redirect('/');
      };
};
controller.save = (req,res) => {
  if(req.session.admin){
    const data=req.body;
       const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/admin/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO admin set ?',[data],(err,admintype)=>{
            res.redirect('/admin');
            });
        });
    };
  } else {
      res.redirect('/');
  };
};


controller.del = (req,res) => {
  if(req.session.admin){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('select * from admin HAVING id = ?',[id],(err,admintype)=>{
          if(err){
              res.json(err);
          }
          res.render('admin/adminDelete',{
            session: req.session,
            data:admintype[0]
          });
      });
  });
} else {
    res.redirect('/');
};
};


controller.delete = (req,res) => {
  if(req.session.admin){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM admin WHERE id= ?',[id],(err,admintype)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(admintype);
            res.redirect('/admin');
        });
    });
  } else {
      res.redirect('/');
  };
};


controller.edit = (req,res) => {
  if(req.session.admin){
    const { id } = req.params;
        req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM admin WHERE id= ?',[id],(err,admintype)=>{
                        res.render('admin/adminUpdate',{
                          session: req.session,
                          data1:admintype[0],
                        });
                });
            });
          } else {
              res.redirect('/');
          };
};

controller.update = (req,res) => {
  if(req.session.admin){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM admin WHERE id= ?',[id],(err,admintype)=>{
                res.render('admin/adminUpdate',{
                  session: req.session,
                  data1:admintype[0],
                });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  admin SET ?  WHERE id = ?',[data,id],(err,admintype) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/admin');
               });
             });
        }
      } else {
          res.redirect('/');
      };
};


controller.add = (req,res) => {
    if(req.session.admin){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM admin',(err,admintype) =>{
      res.render('admin/adminAdd',{
        data:admintype,
        session: req.session
      });
    });
  });
} else {
    res.redirect('/');
};
};
module.exports = controller;
