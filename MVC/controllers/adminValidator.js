
const { check } = require('express-validator');

exports.addValidator = [check('name',"กรุณาใส่ชื่อ").not().isEmpty(),
                        check('username',"กรุณาใส่username").not().isEmpty(),
                        check('password',"กรุณาpassword").not().isEmpty()
                      ];

 exports.editValidator = [check('name',"กรุณาใส่ชื่อ").not().isEmpty(),
                         check('username',"กรุณาใส่username").not().isEmpty(),
                         check('password',"กรุณาpassword").not().isEmpty()
                       ];
