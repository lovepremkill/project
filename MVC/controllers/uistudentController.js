const controller = {};
const { json } = require("body-parser");
const { LOADIPHLPAPI } = require("dns");
const { validationResult } = require("express-validator");
const uuidv4 = require('uuid').v4;

controller.list = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  const st = req.session.student;
  req.getConnection((err, conn) => {
    conn.query("select project_id pj from studentproject where student_id = ?;", [st], (err, studentUser) => {
      if (studentUser.length > 0) {
        conn.query("select t.name nameteh,p.id idp,nameth,nameen,s.firstname names,s.lastname namesl,t.name namet,year,abstract from studentproject sp join project p on sp.project_id=p.id join student s on sp.student_id=s.id join teacher t on p.teacher_id=t.id where sp.project_id = ?;", [studentUser[0].pj], (err, joinProject) => {
          conn.query("select * from studentproject sp join student s on sp.student_id=s.id where sp.project_id = ?;", [studentUser[0].pj], (err, studentproject) => {
            res.render("uistudent/uistudent", { session: req.session, data: joinProject[0], data1: studentproject });
          });
        });
      } else {
        res.render("uistudent/uistudent", { session: req.session, data: null });
      }
    });
  });
}else{
res.redirect('/');
};
};

controller.add = (req, res) => {
    if(req.session.admin || req.session.student || req.session.teacher){
  const data = null;
  req.getConnection((err, conn) => {
    conn.query("SELECT * FROM teacher", (err, teacher) => {
      res.render("uistudent/uistudentAdd", {
        session: req.session,
        data: data,
        data1: teacher,
      });
    });
  });
}else{
res.redirect('/');
};
};

controller.save = (req, res) => {
    if(req.session.admin || req.session.student || req.session.teacher){
  const data = req.body;
  if (data.teacher_id == "") { data.teacher_id = null; }
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    req.getConnection((err, conn) => {
      conn.query("SELECT * FROM teacher", (err, teacher) => {
        res.render("uistudent/uistudentAdd", {
          session: req.session,
          data: data,
          data1: teacher,
        });
      });
    });
  } else {
    req.session.success = true;
    req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query("INSERT INTO project set ?", [data], (err, result) => {
        req.session.saveStudent = result.insertId;
        res.redirect("/uistudent/listAddStudent/" + result.insertId);
      });
    });
  }
}else{
res.redirect('/');
};
};

controller.listAddStudent = (req, res) => {
    if(req.session.admin || req.session.student || req.session.teacher){
  const { id } = req.params;
  req.session.saveStudent = id;
  req.getConnection((err, conn) => {
    conn.query("select th.id idth,nameth,nameen,th.name nameteh,year,abstract from project pj join teacher th on pj.teacher_id=th.id where pj.id =?;", [id], (err, addStudent) => {
      conn.query("select * from studentproject sp join student s on sp.student_id=s.id where sp.project_id = ?;", [id], (err, studentproject) => {
        res.render("uistudent/uistudentListAddStudent", {
          session: req.session,
          data: addStudent[0],
          data1: studentproject,
        });
      });
    }
    );
  });
}else{
res.redirect('/');
};
};

controller.toDepartment = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  req.getConnection((err, conn) => {
    conn.query("SELECT * FROM department", (err, department) => {
      if (err) {
        res.json(err);
      }
      res.render("uistudent/uiSelectDepartment", {
        session: req.session,
        data: department,
      });
    });
  });
}else{
res.redirect('/');
};
};

controller.toMajor = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  const { idDp } = req.params;
  req.getConnection((err, conn) => {
    conn.query(
      "SELECT mj.name name, mj.id id FROM department dp join major mj on mj.department_id=dp.id where dp.id = ?",
      [idDp],
      (err, major) => {
        if (err) {
          res.json(err);
        }
        res.render("uistudent/uiSelectMajor", {
          session: req.session,
          data: major,
        });
      }
    );
  });
}else{
res.redirect('/');
};
};

controller.toStudent = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  const { idMj } = req.params;
  req.getConnection((err, conn) => {
    conn.query(
      "select * from student as s left join studentproject as sj on sj.student_id= s.id where sj.student_id is null and s.major_id = ?",
      [idMj],
      (err, major) => {
        if (err) {
          res.json(err);
        }
        res.render("uistudent/uiSelectStudent", {
          session: req.session,
          data: major,
        });
      }
    );
  });
}else{
res.redirect('/');
};
};



controller.saveStudent = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  const { idSt } = req.params;
  req.getConnection((err, conn) => {
    conn.query("INSERT INTO studentproject set project_id = ? ,student_id = ?", [req.session.saveStudent, idSt], (err, result) => {
      if (err) {
        res.json(err);
      } res.redirect("/uistudent/listAddStudent/" + req.session.saveStudent);
    });
  });
}else{
res.redirect('/');
};
};


controller.edit = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query("select pj.id idpj,teacher_id,th.id idth,nameth,nameen,th.name nameteh,year,abstract from project pj join teacher th on pj.teacher_id=th.id where pj.id =?;", [id], (err, addStudent) => {
      conn.query("SELECT * FROM teacher", (err, teacher) => {
        res.render("uistudent/uistudentUpdate", {
          session: req.session,
          data: addStudent[0],
          data1: teacher,
        });
      });
    });
  });
}else{
res.redirect('/');
};
};

controller.update = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  const errors = validationResult(req);
  const { id } = req.params;
  const data = req.body;
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    req.getConnection((err, conn) => {
      conn.query("select pj.id idpj,teacher_id,th.id idth,nameth,nameen,th.name nameteh,year,abstract from project pj join teacher th on pj.teacher_id=th.id where pj.id =?;", [id], (err, addStudent) => {
        conn.query("SELECT * FROM teacher", (err, teacher) => {
          res.render("uistudent/uistudentUpdate", {
            session: req.session,
            data: addStudent[0],
            data1: teacher,
          });
        });
      });
    });
  } else {
    req.session.success = true;
    req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query(
        "UPDATE  project SET ?  WHERE id = ?",
        [data, id],
        (err, s6123e) => {
          if (err) {
            res.json(err);
          }
          res.redirect("/uistudent");
        }
      );
    });
  }
}else{
res.redirect('/');
};
};

controller.deletelist = (req, res) => {
    if(req.session.admin || req.session.student || req.session.teacher){
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM studentproject WHERE student_id = ?', [id], (err, s6123e) => {
      res.redirect("/uistudent");
    });
  });
}else{
res.redirect('/');
};
};


controller.delete = (req, res) => {
    if(req.session.admin || req.session.student || req.session.teacher){
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM studentproject WHERE project_id = ? and student_id = ?', [req.session.saveStudent, id], (err, s6123e) => {
      res.redirect("/uistudent/listAddStudent/" + req.session.saveStudent);
    });
  });
}else{
res.redirect('/');
};
};


controller.upload = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from project where id = ?;', [id], (err, s6123e) => {
      res.render('uistudent/uistudentUpload', {
        data: id, session: req.session, data1: s6123e[0]
      });
    });
  });
}else{
res.redirect('/');
};
};

controller.uploadUp = (req, res) => {
  if(req.session.admin || req.session.student || req.session.teacher){
  const { id } = req.params;
  const { key } = req.params;
  const data = req.files;

  if (req.files) {
    var file = req.files.filename;
    if (!Array.isArray(file)) {
      var filename = uuidv4() + "." + file.name.split(".")[1];
      file.mv("./public/listProject/" + filename, function (err) {
        if (err) { console.log(err); }
      })
    } else {
      for (var i = 0; i < file.length; i++) {
        var filename = uuidv4() + "." + file[i].name.split(".")[1];
        file[i].mv("./public/listProject/" + filename, function (err) {
          if (err) { console.log(err); }
        })
      }
    }
  }
  req.getConnection((err, conn) => {
    if (key == 1) {
      conn.query('UPDATE project set coverout = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 2) {
      conn.query('UPDATE project set coverin = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 3) {
      conn.query('UPDATE project set certificate = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 4) {
      conn.query('UPDATE project set abstract = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 5) {
      conn.query('UPDATE project set acknowledgment = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 6) {
      conn.query('UPDATE project set contents = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 7) {
      conn.query('UPDATE project set chapter1 = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 8) {
      conn.query('UPDATE project set chapter2 = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 9) {
      conn.query('UPDATE project set chapter3 = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 10) {
      conn.query('UPDATE project set chapter4 = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 11) {
      conn.query('UPDATE project set chapter5 = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 12) {
      conn.query('UPDATE project set refreces = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 13) {
      conn.query('UPDATE project set appendimage = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
    if (key == 14) {
      conn.query('UPDATE project set biography = ? where id = ?', [filename, id], (err, s6123e) => {
        res.redirect('/uistudent/upload/' + id);
      });
    }
  });
}else{
res.redirect('/');
};
}

module.exports = controller;
