const { check } = require('express-validator');
exports.addValidator = [
    check('name', "กรุณาใส่ชื่อโปรแกรมวิชา").not().isEmpty(),
];

exports.updateValidator = [
    check('name', "กรุณาใส่ชื่อโปรแกรมวิชา").not().isEmpty(),
];