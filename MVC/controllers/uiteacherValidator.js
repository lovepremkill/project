const { check } = require('express-validator');
exports.uiteacherValidator = [

];


exports.addValidator = [check('nameth',"กรุณาใส่ชื่อวิจัยภาษาไทย").not().isEmpty(),
check('nameen',"กรุณาใส่ชื่อวิจัยภาษาอังกฤษ").not().isEmpty(),
check('year',"กรุณาใส่ปีการศึกษา").not().isEmpty(),
check('year',"กรุณาใส่ปีการศึกษาเป็นตัวเลข").isInt()
];
