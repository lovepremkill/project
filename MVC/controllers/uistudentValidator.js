const { check } = require('express-validator');

exports.addValidator = [
    check('nameen',"กรุณาใส่ชื่อวิจัย (ภาษาอังกฤษ)").not().isEmpty(),
    check('nameth',"กรุณาใส่ชื่อวิจัย (ภาษาไทย)").not().isEmpty(),
    check('teacher_id',"กรุณาเลือกอาจารย์ที่ปรึกษา").not().isEmpty(),
    check('year',"กรุณาใส่ปีการศึกษาให้ถูกต้อง").isInt()];
