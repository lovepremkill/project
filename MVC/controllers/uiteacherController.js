const controller = {};
const { json } = require("body-parser");
const { LOADIPHLPAPI } = require("dns");
const { validationResult } = require("express-validator");
const uuidv4 = require('uuid').v4;

controller.upload = (req, res) => {
  if(req.session.admin  || req.session.teacher){
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from project where id = ?;', [id], (err, teacher) => {
      res.render('uiteacher/uiteacherUpload', {
        data: id, session: req.session, data1: teacher[0]
      });
    });
  });
}else{
res.redirect('/');
};
};

controller.uploadUp = (req, res) => {
  if(req.session.admin  || req.session.teacher){
  const { id } = req.params;
  const { key } = req.params;
  const data = req.files;

  if (req.files) {
    var file = req.files.filename;
    if (!Array.isArray(file)) {
      var filename = uuidv4() + "." + file.name.split(".")[1];
      file.mv("./public/listProject/" + filename, function (err) {
        if (err) { console.log(err); }
      })
    } else {
      for (var i = 0; i < file.length; i++) {
        var filename = uuidv4() + "." + file[i].name.split(".")[1];
        file[i].mv("./public/listProject/" + filename, function (err) {
          if (err) { console.log(err); }
        })
      }
    }
  }
  req.getConnection((err, conn) => {
    if (key == 1) {
      conn.query('UPDATE project set coverout = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 2) {
      conn.query('UPDATE project set coverin = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 3) {
      conn.query('UPDATE project set certificate = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 4) {
      conn.query('UPDATE project set abstract = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 5) {
      conn.query('UPDATE project set acknowledgment = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 6) {
      conn.query('UPDATE project set contents = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 7) {
      conn.query('UPDATE project set chapter1 = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 8) {
      conn.query('UPDATE project set chapter2 = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 9) {
      conn.query('UPDATE project set chapter3 = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 10) {
      conn.query('UPDATE project set chapter4 = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 11) {
      conn.query('UPDATE project set chapter5 = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 12) {
      conn.query('UPDATE project set refreces = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 13) {
      conn.query('UPDATE project set appendimage = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
    if (key == 14) {
      conn.query('UPDATE project set biography = ? where id = ?', [filename, id], (err, teacher) => {
        res.redirect('/uiteacher/upload/' + id);
      });
    }
  });
}else{
res.redirect('/');
};
}



controller.list = (req,res) => {
  if(req.session.admin  || req.session.teacher){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
    req.getConnection((err,conn) =>{

              conn.query('SELECT * FROM project where teacher_id=?',[teacher1],(err,project)=>{
                conn.query('SELECT sp.project_id as project_id,s.phone as phone,s.studentcode as studentcode,s.firstname as firstname, s.lastname as lastname,m.name as mname,d.name as dname FROM studentproject as sp LEFT JOiN student as s ON sp.student_id=s.id LEFT JOIN major as m ON s.major_id=m.id LEFT JOIN department as d ON m.department_id=d.id ',(err,studentproject)=>{

                res.render('uiteacher/uiteacherList',{session: req.session,admin:admin1,teacher:teacher1,student1:student1,data1:project,data2:studentproject});
              });
          });});
        }else{
        res.redirect('/');
        };
}

controller.detail = (req,res) => {
  if(req.session.admin  || req.session.teacher){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const { id } = req.params;
  req.getConnection((err,conn)=>{
    conn.query('SELECT * FROM teacher WHERE ID=?',[teacher1],(err,teacherid)=>{
              conn.query('SELECT * FROM project WHERE ID=?',[id],(err,project)=>{
                  conn.query('SELECT s.id as id,s.phone as phone,s.studentcode as studentcode,s.firstname as firstname, s.lastname as lastname,m.name as mname,d.name as dname FROM studentproject as sp LEFT JOiN student as s ON sp.student_id=s.id LEFT JOIN major as m ON s.major_id=m.id LEFT JOIN department as d ON m.department_id=d.id where sp.project_id=?',[id],(err,studentproject)=>{
                res.render('uiteacher/uiteacherdetail',{session: req.session,data:teacherid,admin:admin1,teacher:teacher1,student1:student1,data1:project,data2:studentproject});
              });});
});
          });
        }else{
        res.redirect('/');
        };

}

controller.del = (req,res) => {
  if(req.session.admin  || req.session.teacher){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const { id } = req.params;
  req.getConnection((err,conn)=>{
    conn.query('SELECT * FROM teacher WHERE ID=?',[teacher1],(err,teacherid)=>{
              conn.query('SELECT * FROM project WHERE ID=?',[id],(err,project)=>{
                res.render('uiteacher/uiteacherdelete',{session: req.session,data:teacherid,admin:admin1,teacher:teacher1,student1:student1,data1:project});
              });  });
          });
        }else{
        res.redirect('/');
        };

}

controller.delstudent = (req,res) => {
    if(req.session.admin  || req.session.teacher){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const { id } = req.params;

console.log(id);
const sid =id.split("_")[0];
const pid =id.split("_")[1];
console.log(pid);
console.log(sid);
  req.getConnection((err,conn)=>{
              conn.query('SELECT * FROM project WHERE ID=?',[pid],(err,project)=>{
                  conn.query('SELECT * FROM student WHERE ID=?',[sid],(err,student)=>{
                res.render('uiteacher/uiteacherStudentDelete',{session: req.session,admin:admin1,teacher:teacher1,student1:student1,data1:project,data2:student,data3:id});
              });
              });
          });
        }else{
        res.redirect('/');
        };

}

controller.add = (req,res) => {
    if(req.session.admin  || req.session.teacher){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;


    req.getConnection((err,conn) => {

            res.render('uiteacher/uiteacherAdd',{session: req.session,admin:admin1,teacher:teacher1,student1:student1});
  });
}else{
res.redirect('/');
};

  };

  controller.addstudent = (req,res) => {
      if(req.session.admin  || req.session.teacher){
    const admin1 =req.session.admin;
    const teacher1 =req.session.teacher;
    const student1 =req.session.student;
const { id } = req.params;

      req.getConnection((err,conn) => {
        conn.query('SELECT * FROM department ',(err,department)=>{
              res.render('uiteacher/uiteacherAddStudent',{session: req.session,admin:admin1,teacher:teacher1,student1:student1,iddata:id,data1:department});
        });
    });
  }else{
  res.redirect('/');
  };
};

controller.addstudent2 = (req,res) => {
    if(req.session.admin  || req.session.teacher){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const data=req.body;
  console.log(data);
  req.getConnection((err,conn) => {
const { id } = req.params;

      conn.query('SELECT * FROM department ',(err,department)=>{
        conn.query('SELECT * FROM department where id =? ',[data.department_id],(err,department2)=>{
        conn.query('SELECT * FROM major where department_id=? ',[data.department_id],(err,major)=>{
            res.render('uiteacher/uiteacherAddStudent2',{session: req.session,admin:admin1,teacher:teacher1,student1:student1,iddata:id,data:data,data1:department,data2:major,data3:department2});

      });});});
  });
}else{
res.redirect('/');
};
};

controller.addstudent3 = (req,res) => {
    if(req.session.admin  || req.session.teacher){
  const admin1 =req.session.admin;
  const teacher1 =req.session.teacher;
  const student1 =req.session.student;
  const data=req.body;
  console.log(data);
  req.getConnection((err,conn) => {
const { id } = req.params;

      conn.query('SELECT * FROM department ',(err,department)=>{
          conn.query('SELECT * FROM department where id =? ',[data.department_id],(err,department2)=>{
        conn.query('SELECT * FROM major where department_id=? ',[data.department_id],(err,major)=>{
            conn.query('SELECT * FROM major  where id=? ',[data.major_id],(err,major2)=>{
          conn.query('SELECT * FROM student where major_id=? ',[data.major_id],(err,student)=>{
            res.render('uiteacher/uiteacherAddStudent3',{session: req.session,admin:admin1,teacher:teacher1,student1:student1,iddata:id,data:data,data1:department,data2:major,data3:student,data4:department2,data5:major2});

      });});});});});
  });
}else{
res.redirect('/');
};
};

  controller.save = (req,res) => {
    if(req.session.admin  || req.session.teacher){
    const admin1 =req.session.admin;
    const teacher1 =req.session.teacher;
    const student1 =req.session.student;
    console.log("save");
      const data=req.body;

      if(data.teacher_id==""){data.teacher_id = null;}
      const errors = validationResult(req);
          if(!errors.isEmpty()){
              req.session.errors=errors;
              req.session.success=false;
              res.redirect('/uiteacher/add');
          }else{
              req.session.success=true;
              req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                  conn.query('INSERT INTO project set ?',[data],(err,project)=>{
                      res.redirect('/uiteacher');
                  });
              });
          }
        }else{
        res.redirect('/');
        };
  };

  controller.savestudent = (req,res) => {
      if(req.session.admin  || req.session.teacher){
    const admin1 =req.session.admin;
    const teacher1 =req.session.teacher;
    const student1 =req.session.student;
    console.log("save");
    const { id } = req.params;
      const data=req.body;

      if(data.teacher_id==""){data.teacher_id = null;}
      const errors = validationResult(req);
          if(!errors.isEmpty()){
              req.session.errors=errors;
              req.session.success=false;
              res.redirect('/uiteacher/add');
          }else{
              req.session.success=true;
              req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                  conn.query('INSERT INTO studentproject set student_id = ?,project_id= ?',[data.student_id,id],(err,project)=>{
                      res.redirect('/uiteacher');

                  });
              });
          }
        }else{
        res.redirect('/');
        };
  };

  controller.delete = (req,res) => {
    if(req.session.admin  || req.session.teacher){
    const admin1 =req.session.admin;
    const teacher1 =req.session.teacher;
    const student1 =req.session.student;
      const { id } = req.params;
      const  errorss= {errors: [{ value: '', msg: 'ลบข้อมูลนี้ไม่ได้ ', param: '', location: '' }]}/////Update
      req.getConnection((err,conn)=>{
          conn.query('DELETE FROM project WHERE id= ?',[id],(err,problemreport)=>{
              if(err){
                  req.session.errors=errorss;/////Update
                  req.session.success=false;/////Update
              }else{
              req.session.success=true;
              req.session.topic="ลบข้อมูลเสร็จแล้ว";

            }
            res.redirect('/uiteacher')
          });
      });
    }else{
    res.redirect('/');
    };
  };

  controller.deletestudent = (req,res) => {
      if(req.session.admin  || req.session.teacher){
    const admin1 =req.session.admin;
    const teacher1 =req.session.teacher;
    const student1 =req.session.student;
      const { id } = req.params;
      const sid =id.split("_")[0];
      const pid =id.split("_")[1];
      const  errorss= {errors: [{ value: '', msg: 'ลบข้อมูลนี้ไม่ได้ ', param: '', location: '' }]}/////Update
      req.getConnection((err,conn)=>{
          conn.query('DELETE FROM studentproject WHERE project_id = ? && student_id = ?',[pid,sid],(err,problemreport)=>{
              if(err){
                  req.session.errors=errorss;/////Update
                  req.session.success=false;/////Update
              }else{
              req.session.success=true;
              req.session.topic="ลบผู้วิจัยหเสร็จแล้ว";

            }
            res.redirect('/uiteacher')
          });
      });
    }else{
    res.redirect('/');
    };
  };
  controller.edit = (req,res) => {
    if(req.session.admin  || req.session.teacher){
    const admin1 =req.session.admin;
    const teacher1 =req.session.teacher;
    const student1 =req.session.student;
    const { id } = req.params;
    req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM project WHERE ID=?',[id],(err,project)=>{
                  res.render('uiteacher/uiteacherUpdate',{session: req.session,data1:project,admin:admin1,teacher:teacher1,student1:student1});
                });
            });
          }else{
          res.redirect('/');
          };

  }
  controller.update = (req,res) => {
    if(req.session.admin  || req.session.teacher){
    const admin1 =req.session.admin;
    const teacher1 =req.session.teacher;
    const student1 =req.session.student;
    console.log("save");
      const data=req.body;
      const { id } = req.params;

      if(data.teacher_id==""){data.teacher_id = null;}
      const errors = validationResult(req);
          if(!errors.isEmpty()){
              req.session.errors=errors;
              req.session.success=false;
              req.getConnection((err,conn)=>{
                          conn.query('SELECT * FROM project WHERE ID=?',[id],(err,project)=>{
                            res.render('uiteacher/uiteacherUpdate',{session: req.session,data1:project,admin:admin1,teacher:teacher1,student1:student1});
                          });
                      });
          }else{
              req.session.success=true;
              req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                  conn.query('UPDATE project SET ?  WHERE id = ?',[data,id],(err,project)=>{
                      res.redirect('/uiteacher');
                  });
              });
          }
        }else{
        res.redirect('/');
        };
  };
module.exports = controller;
