const express = require('express');
const body = require('body-parser')
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const app = express();
const uuidv4 = require('uuid').v4;
const upload = require("express-fileupload");
app.use(upload());

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(body.urlencoded(express.static('public')));
app.use(cookie());
app.use(session({ secret: 'Passw0rd' }));
app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: 'Passw0rd',
    port: 3306,
    database: 'scitproject'
}, 'single'));


const adminRoute = require('./routes/adminRoute');
app.use('/', adminRoute);

const indexRoute = require('./routes/indexRoute');
app.use('/', indexRoute);

const loginRoute = require('./routes/loginRoute');
app.use('/', loginRoute);

const majorRoute = require('./routes/majorRoute');
app.use('/', majorRoute);

const teachertypeRoute = require('./routes/teachertypeRoute');
app.use('/', teachertypeRoute);

const uilistRoute = require('./routes/uilistRoute');
app.use('/', uilistRoute);

const uiuserdetailRoute = require('./routes/uiuserdetailRoute');
app.use('/', uiuserdetailRoute);

const adminstudentRoute = require('./routes/adminstudentRoute');
app.use('/', adminstudentRoute);

const adminmajorRoute = require('./routes/adminMajorRoute');
app.use('/', adminmajorRoute);

const adminprojectRoute = require('./routes/adminprojectRoute');
app.use('/', adminprojectRoute);

const uiadminteacherRoute = require('./routes/uiadminteacherRoute');
app.use('/', uiadminteacherRoute);

const uistudentRoute = require('./routes/uistudentRoute');
app.use('/', uistudentRoute);

const uiteacherRoute = require('./routes/uiteacherRoute');
app.use('/', uiteacherRoute);

const departmentRoute = require('./routes/departmentRoute');
app.use('/', departmentRoute);
const adminDepartmentRoute = require('./routes/adminDepartmentRoute');
app.use('/', adminDepartmentRoute);


app.listen('8081');
