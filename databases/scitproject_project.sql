-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: scitproject
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nameen` varchar(45) DEFAULT NULL,
  `nameth` varchar(45) DEFAULT NULL,
  `year` int DEFAULT NULL,
  `teacher_id` int DEFAULT NULL,
  `coverout` varchar(45) DEFAULT NULL,
  `coverin` varchar(45) DEFAULT NULL,
  `certificate` varchar(45) DEFAULT NULL,
  `abstract` varchar(2000) DEFAULT NULL,
  `acknowledgment` varchar(45) DEFAULT NULL,
  `contents` varchar(45) DEFAULT NULL,
  `chapter1` varchar(45) DEFAULT NULL,
  `chapter2` varchar(45) DEFAULT NULL,
  `chapter3` varchar(45) DEFAULT NULL,
  `chapter4` varchar(45) DEFAULT NULL,
  `chapter5` varchar(45) DEFAULT NULL,
  `refreces` varchar(45) DEFAULT NULL,
  `appendimage` varchar(10000) DEFAULT NULL,
  `biography` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projectfkteacher_idx` (`teacher_id`),
  CONSTRAINT `projectfkteacher` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'water','น้ำ',2000,1,'pro5.pdf','pro2.pdf','pro4.pdf','บทคัดย่อ1','pro4.pdf','pro1.pdf','pro1.pdf','pro5.pdf','pro5.pdf','pro3.pdf','pro2.pdf','pro5.pdf','บันนานุกรม1','pro7.pdf'),(2,'red','แดง',2003,3,'pro1.pdf','pro7.pdf','pro1.pdf','บทคัดย่อ2','pro7.pdf','pro9.pdf','pro7.pdf','pro5.pdf','pro7.pdf','pro6.pdf','pro8.pdf','pro10.pdf','บันนานุกรม2',NULL),(3,'gogogogo','ไปไปไป',2010,2,'pro10.pdf','pro10.pdf','pro9.pdf','บทคัดย่อ3','pro2.pdf','pro6.pdf','pro3.pdf','pro9.pdf','pro10.pdf','pro8.pdf','pro1.pdf','pro7.pdf','บันนานุกรม3','pro3.pdf'),(4,'cat','แมว',2015,4,'pro8.pdf','pro3.pdf','pro10.pdf','บทคัดย่อ4','pro9.pdf','pro8.pdf','pro7.pdf','pro3.pdf','pro10.pdf','pro8.pdf','pro10.pdf','pro9.pdf','บันนานุกรม4','pro10.pdf');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-05 21:35:20
